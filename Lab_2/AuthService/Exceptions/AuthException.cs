﻿using CentralServiceLib.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuthService.Exceptions
{
    public class AuthException : DBException
    {
        public AuthException() : base()
        {

        }

        public AuthException(string message) : base(message)
        {

        }
    }
}
