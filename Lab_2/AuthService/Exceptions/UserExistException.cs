﻿using CentralServiceLib.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;

namespace AuthService.Exceptions
{
    public class UserExistException : DBException
    {
        public UserExistException() : base()
        {

        }

        public UserExistException(string message) : base(message)
        {

        }
    }
}
