﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CSLib.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using AuthService.Model.Services;
using Newtonsoft.Json;
using CentralServiceLib.Exceptions;
using CentralServiceLib;
using AuthService.Exceptions;

namespace AuthService.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private IAuthService authService;

        public AuthController(IAuthService service)
        {
            if(service == null)
            {
                throw new ArgumentNullException();
            }
            authService = service;
        }

        // GET: api/auth
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] string token)
        {
            Console.WriteLine("Received data in auth get method: ");
            Console.WriteLine(token);

            TokenData data = new TokenData(token);
            RegistrData rd;
            try
            {
                rd = await authService.GetRegData(data);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return processException(e);
            }

            Console.WriteLine("Sended data in auth get method: ");
            Console.WriteLine(JsonConvert.SerializeObject(rd));

            return Ok(rd);
        }

        // POST: api/auth
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] AuthData data)
        {
            Console.WriteLine("Received data in auth post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            TokenData td;
            try
            {
                td = await authService.Authorize(data);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return processException(e);
            }

            Console.WriteLine("Sended data in auth post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(td));

            return Ok(td);
        }

        private IActionResult processException(Exception exc)
        {
            try
            {
                throw exc;
            }
            catch (AuthException e)
            {
                return Unauthorized(new Error(e.Message));
            }
            catch (IncorrectDataException e)
            {
                return BadRequest(new Error(e.Message));
            }
            catch (Exception e)
            {
                return StatusCode(500, new Error(e.Message));
            }
        }
    }
}
