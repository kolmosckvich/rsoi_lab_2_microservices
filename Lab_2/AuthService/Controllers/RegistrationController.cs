﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthService.Exceptions;
using AuthService.Model.Services;
using CentralServiceLib;
using CentralServiceLib.Exceptions;
using CSLib.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AuthService.Controllers
{
    [Route("api/reg")]
    [ApiController]
    public class RegistrationController : ControllerBase
    {
        private IRegisterService regService;

        public RegistrationController(IRegisterService service)
        {
            if (service == null)
            {
                throw new ArgumentNullException();
            }
            regService = service;
        }

        // POST: api/reg
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] RegistrData data)
        {
            Console.WriteLine("Received data in reg post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            TokenData td;
            try
            {
                td = await regService.Register(data);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return processException(e);
            }

            Console.WriteLine("Sended data in reg post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(td));

            return Ok(td);
        }

        private IActionResult processException(Exception exc)
        {
            try
            {
                throw exc;
            }
            catch(UserExistException e)
            {
                return BadRequest(new Error(e.Message));
            }
            catch (IncorrectDataException e)
            {
                return BadRequest(new Error(e.Message));
            }
            catch (Exception e)
            {
                return StatusCode(500, new Error(e.Message));
            }
        }
    }
}
