﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AuthService.Model;
using AuthService.Model.Managers;
using AuthService.Model.Services;
using AuthService.ViewModels.OAuth;
using CentralServiceLib;
using CSLib.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace AuthService.Controllers
{
    [Route("api/oauth")]
    [ApiController]
    public class OAuthController : Controller
    {
        private IAuthService authService;

        public OAuthController(IAuthService service)
        {
            if (service == null)
            {
                throw new ArgumentNullException();
            }
            authService = service;
        }

        [NonAction]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            
            return;
        }

        // GET: api/oauth
        [HttpGet]
        public async Task<IActionResult> OAuth([FromQuery] string redirUrl)
        {
            Console.WriteLine("Received data in oauth get method: ");
            Console.WriteLine(redirUrl);

            OAuthViewModel oAuthViewModel = new OAuthViewModel { RedirUrl = redirUrl };
            return View(oAuthViewModel);
        }

        // POST: api/oauth
        [HttpPost]
        public async Task<IActionResult> OAuth([FromForm]OAuthViewModel oAuthViewModel)
        {
            Console.WriteLine("Started oauth post method");
            if(ModelState.IsValid)
            {

            }
            try
            {
                string username = oAuthViewModel.Username;
                string password = oAuthViewModel.Password;
                string redirUrl = oAuthViewModel.RedirUrl;
                var td = await authService.Authorize(new AuthData() { Username = username, Password = password });
                var ad = TokenManager.GetAuthData(td);
                string ct = OCTokenManager.RegisterUserToken(ad);
                return Redirect(redirUrl+$"?code={ct}");
            }
            catch(Exception e)
            {
                
            }

            return View(oAuthViewModel);
        }

        // GET: api/oauth/token
        [HttpPost("token")]
        public async Task<IActionResult> Auth([FromBody] TokenData data)
        {
            string code = data.Token;
            Console.WriteLine("Received data in oauth auth method: ");
            Console.WriteLine(code);

            if (OCTokenManager.CheckUser(data.Token))
            {
                var ad = OCTokenManager.GetAuthData(data);
                string ot = OTTokenManager.RegisterUserToken(ad);
                string or = ORTokenManager.RegisterUserToken(ad);
                return Ok(new OTokenData(ot, or));
            }
            else
            {
                return Unauthorized(new Error(""));
            }
            
        }

        // GET: api/oauth/refresh
        [HttpPost("refresh")]
        public async Task<IActionResult> Refresh([FromBody] OTokenData data)
        {
            string refresh = data.RefreshToken;
            Console.WriteLine("Received data in oauth refresh method: ");
            Console.WriteLine(refresh);

            if (ORTokenManager.CheckUser(data.RefreshToken))
            {
                var td = new TokenData(data.RefreshToken);
                var ad = ORTokenManager.GetAuthData(td);
                string ot = OTTokenManager.RegisterUserToken(ad);
                string or = ORTokenManager.RegisterUserToken(ad);
                return Ok(new OTokenData(ot, or));
            }
            else
            {
                return Unauthorized(new Error(""));
            }

        }

        private IActionResult processException(Exception exc)
        {
            try
            {
                throw exc;
            }
            catch (Exception e)
            {
                return StatusCode(500, new Error(e.Message));
            }
        }

    }
}
