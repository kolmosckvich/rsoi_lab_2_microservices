﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthService.Model
{
    [JsonObject]
    public class OTokenData
    {
        [JsonProperty("Token")]
        public string Token { get; set; }

        [JsonProperty("Expires")]
        public int Expires { get; set; }

        [JsonProperty("RefreshToken")]
        public string RefreshToken { get; set; }

        public OTokenData(string token, string refreshToken)
        {
            Token = token;
            RefreshToken = refreshToken;
            Expires = (Int32)TimeSpan.FromMinutes(30).TotalMilliseconds;
        }
    }
}
