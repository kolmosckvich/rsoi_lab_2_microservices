﻿using AuthService.Exceptions;
using CentralServiceLib;
using CSLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace AuthService.Model.Managers
{
    public static class ORTokenManager
    {
        private static Dictionary<string, DateTime> tokenList = new Dictionary<string, DateTime>();
        private static Dictionary<string, AuthData> authDataList = new Dictionary<string, AuthData>();

        private static object locker = new object();

        static ORTokenManager()
        {

        }

        public static string RegisterUserToken(AuthData data)
        {
            DateTime time = DateTime.Now;
            string token = Hash.HashId(data.Username, time);

            lock (locker)
            {
                if (tokenList.Keys.Contains(token))
                {
                    tokenList[token] = time;
                }
                else
                {
                    AddToken(token, data);
                }
            }

            return token;
        }

        public static bool CheckUser(string token)
        {
            bool checkRes = false;

            lock (locker)
            {
                if (tokenList.Keys.Contains(token))
                {
                    //Обновление при обращении
                    //tokenList[token] = DateTime.Now;
                    checkRes = true;
                }
            }

            return checkRes;
        }

        private static void AddToken(string token, AuthData data)
        {
            lock (locker)
            {
                tokenList.Add(token, DateTime.Now);
                authDataList.Add(token, data);
            }
        }

        private static void RemoveUser(string token)
        {
            lock (locker)
            {
                tokenList.Remove(token);
                authDataList.Remove(token);
            }
        }

        public static AuthData GetAuthData(TokenData tdata)
        {
            AuthData data = null;
            string token = tdata.Token;
            lock (locker)
            {
                if (tokenList.ContainsKey(token))
                {
                    data = authDataList[token];
                    //tokenList[token] = DateTime.Now;
                }
                else
                {
                    throw new AuthException("Такого токена нет в списке пользователей");
                }
            }
            return data;
        }
    }
}
