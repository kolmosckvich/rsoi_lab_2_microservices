﻿using AuthService.Exceptions;
using CentralServiceLib;
using CentralServiceLib.Exceptions;
using CSLib.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace AuthService.Model.Managers
{
    public class AuthDBManager : DBManager
    {
        public AuthDBManager(string _connectionString) : base(_connectionString)
        {

        }

        public void AddUser(RegistrData data)
        {
            if (CheckUserName(data.Username))
            {
                throw new UserExistException("User with this username already exist");
            }
            if (CheckUserPassport(data.Passport))
            {
                throw new UserExistException("User with this passport number already exist");
            }
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string cmdstr = $"INSERT INTO Users (Username, Password, FirstName, SecondName, Passport) " +
                                    $"VALUES ('{data.Username}', '{data.Password}', '{data.FirstName}'," +
                                    $" '{data.SecondName}', '{data.Passport}')";
                    var cmd = new SqlCommand(cmdstr, connection);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw new DBException(e.Message);
                    }
                    connection.Close();
                }
            }
            catch(Exception e)
            {
                throw new DBException(e.Message);
            }
            
        }

        public RegistrData GetUser(AuthData data)
        {
            RegistrData rd;
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string cmdstr = $"SELECT * FROM Users WHERE Username = '{data.Username}' AND Password = '{data.Password}'";
                    var cmd = new SqlCommand(cmdstr, connection);
                
                    SqlDataReader rdr = cmd.ExecuteReader();
                    if(!rdr.HasRows)
                    {
                        throw new AuthException("Incorret credentials");
                    }
                    else if(rdr.RecordsAffected > 1)
                    {
                        throw new DBException("Finded more than 1 user with current credentials!");
                    }
                    rdr.Read();
                    rd = new RegistrData(data.Username, data.Password,
                                                        rdr["FirstName"].ToString(),
                                                        rdr["SecondName"].ToString(),
                                                        rdr["Passport"].ToString());

                    
                    connection.Close();
                }
            }
            catch (AuthException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new DBException(e.Message);
            }
            return rd;
        }

        private bool CheckUserName(string username)
        {
            bool users = false;
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string cmdstr = $"SELECT * FROM Users WHERE Username = '{username}'";
                    var cmd = new SqlCommand(cmdstr, connection);

                    users = cmd.ExecuteReader().HasRows;
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                throw new DBException(e.Message);
            }
            return users;
        }

        private bool CheckUserPassport(string passport)
        {
            bool users = false;
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string cmdstr = $"SELECT * FROM Users WHERE Passport = '{passport}'";
                    var cmd = new SqlCommand(cmdstr, connection);

                    users = cmd.ExecuteReader().HasRows;
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                throw new DBException(e.Message);
            }
            return users;
        }
    }
}
