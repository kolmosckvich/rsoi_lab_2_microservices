﻿using CSLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthService.Model.Services
{
    public interface IAuthService
    {
        Task<TokenData> Authorize(AuthData data);

        Task<RegistrData> GetRegData(TokenData data);
    }
}
