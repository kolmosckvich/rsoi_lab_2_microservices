﻿using AuthService.Exceptions;
using AuthService.Model.Managers;
using CentralServiceLib.Exceptions;
using CSLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthService.Model.Services
{
    public class RegistrationService : IRegisterService
    {
        private AuthDBManager dbManager;

        public RegistrationService()
        {
            string connectionString = AppConfiguration.ConnectionString;
            dbManager = new AuthDBManager(connectionString);
        }

        public async Task<TokenData> Register(RegistrData data)
        {
            checkData(data);
            dbManager.AddUser(data);
            string token = TokenManager.RegisterUserToken(
                                                          new AuthData()
                                                          {
                                                              Username = data.Username,
                                                              Password = data.Password
                                                          });
            return new TokenData(token);
        }

        private void checkData(RegistrData data)
        {
            string excMsg = "";
            if(data.FirstName == "")
            {
                excMsg += "FirstName must be have at least 1 character length!\n";
            }
            if (data.SecondName == "")
            {
                excMsg += "SecondName must be have at least 1 character length!\n";
            }
            if (data.Passport == "")
            {
                excMsg += "Passport must be have at least 1 character length!\n";
            }
            if (data.Password == "")
            {
                excMsg += "Password must be have at least 1 character length!\n";
            }
            if (data.Username == "")
            {
                excMsg += "Username must be have at least 1 character length!\n";
            }
            if(excMsg != "")
                throw new IncorrectDataException(excMsg);
        }
    }
}
