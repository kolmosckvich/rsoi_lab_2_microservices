﻿using AuthService.Model.Managers;
using CSLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AuthService.Model.Services
{
    public class AuthentificationService : IAuthService
    {
        private AuthDBManager dbManager;

        public AuthentificationService()
        {
            string connectionString = AppConfiguration.ConnectionString;
            dbManager = new AuthDBManager(connectionString);
        }

        public async Task<TokenData> Authorize(AuthData data)
        {
            dbManager.GetUser(data);
            string token = TokenManager.RegisterUserToken(data);
            return new TokenData(token);
        }

        public async Task<RegistrData> GetRegData(TokenData data)
        {
            AuthData ad;
            try
            {
                ad = TokenManager.GetAuthData(data);
            }
            catch(Exception e)
            {
                ad = OTTokenManager.GetAuthData(data);
            }
            var regData = dbManager.GetUser(ad);
            return regData;
        }
    }
}
