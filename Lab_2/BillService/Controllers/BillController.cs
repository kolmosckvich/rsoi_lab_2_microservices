﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BillService.Model.Managers;
using BillService.Model.Services;
using CentralServiceLib.Data;
using CSLib.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;

namespace BillService.Controllers
{
    [Route("api/bill")]
    [ApiController]
    public class BillController : Controller
    {
        private IBillService billService;

        public BillController(IBillService service)
        {
            if (service == null)
            {
                throw new ArgumentNullException();
            }
            billService = service;
        }

        [NonAction]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.RouteData.Values["action"].ToString() == "Auth")
                return;
            var headerValue = Request.Headers["MyAuth"];
            if (headerValue.Any() == false) context.Result = Unauthorized();
            var person = headerValue.ToString();
            if (!TokenManager.CheckUser(person)) context.Result = Unauthorized();
            return;
        }

        [HttpPost("auth")]
        public async Task<IActionResult> Auth([FromBody] AuthData data)
        {
            Console.WriteLine("Received data in auth post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            TokenData td = new TokenData("");
            try
            {
                if (data.Username == AppConfiguration.GatewayAppID && data.Password == AppConfiguration.GatewayAppSecret)
                {
                    string token = TokenManager.RegisterUserToken(data);
                    td = new TokenData(token);
                }
                else return Unauthorized();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");
            }

            Console.WriteLine("Sended data in auth post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(td));

            return Ok(td);
        }

        // POST: api/bill
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] BillData data)
        {
            Console.WriteLine("Received data in bill post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            try
            {
                billService.DoBillTransaction(data);

                Console.WriteLine("Ended bill post with 200");

                return Ok();
            }
            catch(Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return StatusCode(500);
            }
        }
    }
}
