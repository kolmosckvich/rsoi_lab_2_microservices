﻿using BillService.Model.Managers;
using CentralServiceLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillService.Model.Services
{
    public class BillingService : IBillService
    {
        private BillDBManager dbManager;

        public BillingService()
        {
            string connectionString = AppConfiguration.ConnectionString;
            dbManager = new BillDBManager(connectionString);
        }

        public void DoBillTransaction(BillData data)
        {
            dbManager.AddBillData(data);
        }
    }
}
