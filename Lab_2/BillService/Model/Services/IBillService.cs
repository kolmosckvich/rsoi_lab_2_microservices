﻿using CentralServiceLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillService.Model.Services
{
    public interface IBillService
    {
        void DoBillTransaction(BillData data);
    }
}
