﻿using CentralServiceLib;
using CSLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BillService.Model.Managers
{
    public static class TokenManager
    {
        private static Dictionary<string, DateTime> tokenList = new Dictionary<string, DateTime>();

        private static object locker = new object();
        private static TimeSpan timeout = TimeSpan.FromMinutes(1);

        static TokenManager()
        {
            TimerCallback tm = new TimerCallback(ClearOldUsers);
            int time = (int)timeout.TotalMilliseconds;

            Timer timer = new Timer(tm, null, 0, time);
        }

        public static string RegisterUserToken(AuthData data)
        {
            DateTime time = DateTime.Now;
            string token = Hash.HashId(data.Username, time);

            lock (locker)
            {
                if (tokenList.Keys.Contains(token))
                {
                    tokenList[token] = time;
                }
                else
                {
                    AddToken(token, data);
                }
            }

            return token;
        }

        public static bool CheckUser(string token)
        {
            bool checkRes = false;

            lock (locker)
            {
                if (tokenList.Keys.Contains(token))
                {
                    checkRes = true;
                }
            }

            return checkRes;
        }

        private static void ClearOldUsers(object obj)
        {
            lock (locker)
            {
                DateTime now = DateTime.Now;
                List<string> keysToRemove = new List<string>();

                foreach (var pair in tokenList)
                {
                    if ((now - pair.Value).TotalMilliseconds > timeout.TotalMilliseconds)
                    {
                        keysToRemove.Add(pair.Key);
                    }
                }

                for (int i = 0; i < keysToRemove.Count; i++)
                {
                    RemoveUser(keysToRemove[i]);
                }

                keysToRemove.Clear();
            }
        }

        private static void AddToken(string token, AuthData data)
        {
            lock (locker)
            {
                tokenList.Add(token, DateTime.Now);
            }
        }

        private static void RemoveUser(string token)
        {
            lock (locker)
            {
                tokenList.Remove(token);
            }
        }
    }
}
