﻿using CentralServiceLib;
using CentralServiceLib.Data;
using CentralServiceLib.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace BillService.Model.Managers
{
    public class BillDBManager : DBManager
    {
        public BillDBManager(string _connectionString) : base(_connectionString)
        {

        }

        public void AddBillData(BillData data)
        {
            data.DateTime = DateTime.Now.ToString("g");
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string cmdstr = $"INSERT INTO Transactions (Username, Summ, DateTime, Card) " +
                                    $"VALUES ('{data.Passport}', '{data.Summ}', '{data.DateTime}'," +
                                    $" '{data.Card}')";
                    var cmd = new SqlCommand(cmdstr, connection);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw new DBException(e.Message);
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                throw new DBException(e.Message);
            }

        }
    }
}
