﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BillService
{
    public static class AppConfiguration
    {
        public static string ConnectionString { get; set;}

        public static string GatewayAppID = "123";
        public static string GatewayAppSecret = "777";
    }
}
