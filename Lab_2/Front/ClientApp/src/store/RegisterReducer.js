﻿import { REG, REG_SUCCESS, REG_ERROR, REG_ERROR_CONFIRM } from '../actions/ActionTypes.js';
import { dispatch } from 'react-redux';

const GATEWAY_IP = 'http://127.0.0.1:5000/gateway';
const AUTH_IP = 'http://127.0.0.1:5002/api/auth';

const initialState = {
    error: '',
    info: '',
};

export const actionCreators = {
    reg_err: (error) => ({ type: REG_ERROR, error: error }),
    reg_succ: (data) => ({ type: REG_SUCCESS }),
    reg_conf: () => ({ type: REG_ERROR_CONFIRM }),
};

export const reducer = (state = initialState, action) => {
    state = state || initialState;

    if (action.type === REG_SUCCESS) {
        return {...state, error: '', info: 'Успешная регистрация'};
    }

    if (action.type === REG_ERROR) {
        return { ...state, error: action.error, info: '' };
    }

    if (action.type === REG_ERROR_CONFIRM) {
        return { ...state, error: '', info: '' };
    }

    return state;
};