﻿import { AUTH, OAUTH_REQ, AUTH_SUCCESS, AUTH_ERROR, AUTH_ERROR_CONFIRM } from '../actions/ActionTypes.js';

const initialState = {
    error: '',
    info: '',
    Token: '',
    Username: '',
    RefreshToken: '',
    Expires: ''
};

export const actionCreators = {
    auth_err: (error) => ({ type: AUTH_ERROR, error: error }),
    auth_succ: (data, username) => ({ type: AUTH_SUCCESS, data: data, username: username }),
    auth_conf: () => ({ type: AUTH_ERROR_CONFIRM }),
    oauth_req: (data) => ({ type: OAUTH_REQ , data: data}),
};

export const reducer = (state = initialState, action) => {
    state = state || initialState;

    if (action.type === AUTH_SUCCESS) {
        return { ...state, error: '', Token: action.data.Token, Username: action.username }
    }

    if (action.type === AUTH_ERROR) {
        return { ...state, error: action.error}
    }

    if (action.type === AUTH_ERROR_CONFIRM) {
        return { ...state, error:'' }
    }

    if (action.type === OAUTH_REQ) {
        return {
            ...state, error: '',
            Token: action.data.Token,
            Refresh_token: action.data.RefreshToken,
            Expires_in: action.data.Expires,
        }
    }

    return state;
};