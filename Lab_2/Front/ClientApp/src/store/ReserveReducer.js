﻿import { RES, RES_SUCCESS, RES_ERROR, RES_ERROR_CONFIRM, RES_HALF } from '../actions/ActionTypes.js';

const GATEWAY_IP = 'http://127.0.0.1:5000/gateway';

const initialState = {
    error: '',
    info: ''
};

export const actionCreators = {
    res_err: (error) => ({ type: RES_ERROR, error: error }),
    res_succ: (data) => ({ type: RES_SUCCESS }),
    res_conf: () => ({ type: RES_ERROR_CONFIRM }),
    res_half: () => ({ type: RES_HALF }),
};

export const reducer = (state = initialState, action) => {
    state = state || initialState;

    if (action.type === RES_SUCCESS) {
        return { ...state, error: ''};
    }

    if (action.type === RES_ERROR) {
        return { ...state, error: action.error };
    }

    if (action.type === RES_ERROR_CONFIRM) {
        return { ...state, error: '' };
    }

    if (action.type === RES_HALF) {
        return { ...state, info: action.info };
    }

    return state;
};