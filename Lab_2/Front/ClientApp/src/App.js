import React from 'react';
import {
        Route,
        Switch,
       } from 'react-router-dom';
import Layout from './components/Layout';
import Home from './components/Home';
import Register from './components/Register';
import Reserve from './components/Reserve';
import Auth from './components/Auth';
import Tickets from './components/Tickets';
import NotFound from './components/NotFound';
import OAuth from './components/OAuth';

export default () => (
        <div>
            <Route component={Layout} />
            <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/register' component={Register} />
                <Route exact path='/reserve' component={Reserve} />
                <Route exact path='/auth' component={Auth} />
                <Route exact path='/oacallback' component={OAuth} />
                <Route exact path='/tickets' component={Tickets} />
                <Route path='*' component={NotFound} />
            </Switch>
        </div>
);
