﻿export const AUTH = "AUTH";
export const AUTH_SUCCESS = "AUTH_SUCCESS";
export const AUTH_ERROR = "AUTH_ERROR";
export const AUTH_ERROR_CONFIRM = "AUTH_ERROR_CONFIRM";

export const REG = "REG";
export const REG_SUCCESS = "REG_SUCCESS";
export const REG_ERROR = "REG_ERROR";
export const REG_ERROR_CONFIRM = "REG_ERROR_CONFIRM";

export const RES = "RES";
export const RES_SUCCESS = "RES_SUCCESS";
export const RES_ERROR = "RES_ERROR";
export const RES_ERROR_CONFIRM = "RES_ERROR_CONFIRM";
export const RES_HALF = "RES_HALF";

export const OAUTH_REQ = "OAUTH_REQ";