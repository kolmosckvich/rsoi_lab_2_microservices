﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/TicketsReducer';
import Info from '../components/Info';

class Tickets extends Component {

    constructor(props) {
        super(props);

        this.state = {
            choosedItem: 0,
            tickets: []
        };

        this.itemChoose = this.itemChoose.bind(this);
    }

    numbers = [1, 2, 3, 4, 5];

    listItems(numbers) {
        return numbers.map((number) => {
            let disabled = this.isDisabled(number);
            let active = (number == this.state.choosedItem);
            let basetype = "list-group-item list-group-item-action list-group-item-success";
            return <button type="button" class={basetype} id={number.toString()}
                active={active} disabled={disabled} onClick={this.itemChoose}>{number}</button>
        }
        );
    }   

    isDisabled(number) {
        if (number == 2)
            return true
    }

    itemChoose(event) {
        this.state.choosedItem = event.target.id;
    }

    render() {
        let errWin = this.state.error != '';
        let errTxt = this.state.error;
        return (
            <div>
                <Info open={errWin} isError={errWin} text={errTxt} onClose={this.alertClosed} />
                <h2>Билеты</h2>
                <div class="container">
                    <div class="list-group">{this.listItems(this.numbers)}</div>
                </div>
            </div >
        );
    }
}

export default connect(
    state => state.tickets,
    dispatch => bindActionCreators(actionCreators, dispatch)
)(Tickets);
