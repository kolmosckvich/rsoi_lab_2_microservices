﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/RegisterReducer';
import { actionCreators as authActionCreators } from '../store/AuthReducer';
import Info from '../components/Info';

const REG_IP = 'http://127.0.0.1:5002/api/reg';
const md5 = require("blueimp-md5");

class Register extends Component {

    constructor(props) {
        super(props);

        this.state = {
            error: '',
            regData: {
                Username: '',
                Password: '',
                FirstName: '',
                SecondName: '',
                Passport: ''
            }
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.alertClosed = this.alertClosed.bind(this);
    }

    alertClosed(event) {
        this.props.actions.reg_conf();
    }

    handleChange(event) {
        const field = event.target.name;
        const regData = this.state.regData;
        regData[field] = event.target.value;
        return this.setState({ regData: regData });
    }

    handleSubmit(event) {
        event.preventDefault();
        let regData = {
            Username: this.state.regData.Username,
            Password: this.state.regData.Password,
            FirstName: this.state.regData.FirstName,
            SecondName: this.state.regData.SecondName,
            Passport: this.state.regData.Passport
        }
        this.register(regData);
    }

    register(regData) {
        let rd = regData;
        fetch(`${REG_IP}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Username: rd.Username,
                Password: md5(rd.Password),
                FirstName: rd.FirstName,
                SecondName: rd.SecondName,
                Passport: rd.Passport
            })
        }).then(response => {
            if (response.ok)
                return response.json();
            else
                throw response;
        }).then(data => {
            this.props.actions.reg_succ(data);
            this.props.actions.auth_succ(data, rd.Username);
            this.props.history.push("/reserve");
        }).catch(response => {
            response.json().then(msg => {
                let text = '';
                if (!(typeof msg.Message === 'undefined')) {
                    text = " | " + msg.Message;
                }
                let error = { message: response.status + " " + response.statusText + text }
                console.log(error.message);
                this.props.actions.reg_err(error.message);
            });
        })
    }

    render() {
        let infoWin = this.props.state.register.info != '';
        let errWin = this.props.state.register.error != '';
        let errTxt = '';
        if (!errWin) {
            errTxt = this.props.state.register.info;
        }
        else {
            errTxt = this.props.state.register.error;
        }
        return (
            <div>
                <Info open={errWin || infoWin} isError={errWin} text={errTxt} onClose={this.alertClosed} />
                <h2>Регистрация</h2>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-4">
                            <div class="border shadow-lg p-3 mb-5 bg-light rounded">
                                <form onSubmit={this.handleSubmit} className="needs-validation">
                                    <div class="form-group">
                                        <label for="InputUsername">Имя пользователя</label>
                                        <input type="text" class="form-control" id="InputUsername"
                                            name="Username" placeholder="Введите имя пользователя"
                                            value={this.state.regData.Username} onChange={this.handleChange} required
                                        />
                                    </div>
                                    <div class="form-group">
                                        <label for="InputPassword">Пароль</label>
                                        <input type="password" class="form-control" id="InputPassword"
                                            name="Password" placeholder="Введите пароль"
                                            value={this.state.regData.Password} onChange={this.handleChange} required
                                        />
                                    </div>
                                    <div class="form-group">
                                        <label for="InputFirstName">Имя</label>
                                        <input type="text" class="form-control" id="InputFirstName"
                                            name="FirstName" placeholder="Введите имя"
                                            value={this.state.regData.FirstName} onChange={this.handleChange} required
                                        />
                                    </div>
                                    <div class="form-group">
                                        <label for="InputSecondName">Фамилия</label>
                                        <input type="text" class="form-control" id="InputSecondName"
                                            name="SecondName" placeholder="Введите фамилию"
                                            value={this.state.regData.SecondName} onChange={this.handleChange} required
                                        />
                                    </div>
                                    <div class="form-group">
                                        <label for="InputPassport">Паспорт</label>
                                        <input type="text" class="form-control" id="InputPassport"
                                            name="Passport" placeholder="Введите номер паспорта"
                                            value={this.state.regData.Passport} onChange={this.handleChange} required
                                        />
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-primary">
                                            Зарегистрироваться
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default connect(
    (state) => {
        const { register, auth } = state;
        return {
            state: { register, auth }
        };
    },
    (dispatch) => {
        return {
            actions: bindActionCreators(Object.assign({}, actionCreators, authActionCreators), dispatch)
        };
    }
)(Register);
