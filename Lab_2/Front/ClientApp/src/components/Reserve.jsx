﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/ReserveReducer';
import Auth from '../components/Auth';
import Info from '../components/Info';

const GATEWAY_IP = 'http://127.0.0.1:5000/gateway/reserve';
const TOKEN_IP = 'http://127.0.0.1:5000/gateway/ticket/token';
const CRED_IP = 'http://127.0.0.1:5000/gateway/ticket/cred';

class Reserve extends Component {

    constructor(props) {
        super(props);

        this.state = {
            error: '',
            resData: {
                SeatID: '',
                Card: '',
                Summ: '',
                FirstName: '',
                SecondName: '',
                Passport: ''
            },
            isAuthorized: false,
            choosedItem: 0,
            seats: [],
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.alertClosed = this.alertClosed.bind(this);
        this.itemChoose = this.itemChoose.bind(this);

        this.loadSeats();

    }

    listItems(seats) {
        return seats.map((seat) => {
            let disabled = this.isDisabled(seat);
            let active = (seat.SeatID == this.state.choosedItem);
            let basetype = "list-group-item list-group-item-action list-group-item-success";
            if (active) {
                basetype = "list-group-item list-group-item-action list-group-item-info"
            }
            return <button type="button" class={basetype} id={seat.SeatID} key={seat.SeatID}
                disabled={disabled} onClick={this.itemChoose}>{seat.SeatID}</button>
        }
        );
    }

    isDisabled(seat) {
        if (seat.Reserved)
            return true;
        else
            return false;
    }

    itemChoose(event) {
        this.state.choosedItem = event.target.id;
        event.target.class = "list-group-item list-group-item-action list-group-item-info";
        this.forceUpdate(); 
    }

    alertClosed(event) {
        this.props.actions.res_conf();
    }

    handleChange(event) {
        const field = event.target.name;
        const resData = this.state.resData;
        resData[field] = event.target.value;
        return this.setState({ resData: resData });
    }

    handleSubmit(event) {
        event.preventDefault();
        if (event.target.id == "1") {
            return;
        }
        if (this.state.choosedItem < 1) {
            this.props.actions.res_err("Пожалуйста, выберите место");
        }
        else {
            if (this.props.state.auth.Token == '') {
                let rd = {
                    FirstName: this.state.resData.FirstName,
                    SecondName: this.state.resData.SecondName,
                    Passport: this.state.resData.Passport,
                    SeatID: this.state.choosedItem,
                    Card: this.state.resData.Card,
                    Summ: this.state.resData.Summ,
                }

                fetch(`${CRED_IP}`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/pdf',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        FirstName: rd.FirstName,
                        SecondName: rd.SecondName,
                        Passport: rd.Passport,
                        SeatID: rd.SeatID,
                        Card: rd.Card,
                        Summ: rd.Summ,
                    })
                }).then(response => {
                    if (response.ok) {
                        this.state.seats[rd.SeatID - 1].Reserved = true;
                        this.state.choosedItem = 0;
                        return response.body;
                    }
                    else
                        throw response;
                }).then(stream => new Response(stream))
                  .then(response => response.blob())
                  .then(data => {
                    this.props.actions.res_succ(data);
                    this.processPdf(data);
                    this.loadSeats();
                  }).catch(response => {
                      response.json().then(msg => {
                          let text = '';
                          if (!(typeof msg.Message === 'undefined')) {
                              text = " | " + msg.Message;
                          }
                          let error = { message: response.status + " " + response.statusText + text }
                          console.log(error.message);
                          this.props.actions.res_err(error.message);
                      });
                  })
            }
            else {
                let rd = {
                    Token: this.props.state.auth.Token,
                    SeatID: this.state.choosedItem,
                    Card: this.state.resData.Card,
                    Summ: this.state.resData.Summ,
                }

                fetch(`${TOKEN_IP}`, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/pdf',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        Token: rd.Token,
                        SeatID: rd.SeatID,
                        Card: rd.Card,
                        Summ: rd.Summ,
                    })
                }).then(response => {
                    if (response.ok) {
                        this.state.seats[rd.SeatID - 1].Reserved = true;
                        this.state.choosedItem = 0;
                        return response.body;
                    }
                    else
                        throw response;
                }).then(stream => new Response(stream))
                  .then(response => response.blob())
                  .then(data => {
                        this.props.actions.res_succ(data);
                        this.processPdf(data);
                        this.loadSeats();
                  }).catch(response => {
                      response.json().then(msg => {
                          let text = '';
                          if (!(typeof msg.Message === 'undefined')) {
                              text = " | " + msg.Message;
                          }
                          let error = { message: response.status + " " + response.statusText + text }
                          console.log(error.message);
                          this.props.actions.res_err(error.message);
                      });
                  })
            }
        }

    }

    processPdf(pdf_b) {
        var newBlob = new Blob([pdf_b], { type: "application/pdf" })
        const data = window.URL.createObjectURL(newBlob);
        window.open(data);
    }

    loadSeats() {
        fetch(`${GATEWAY_IP}`, {
            method: 'GET',
            headers: {
            },
        }).then(response => {
            if (response.ok)
                return response.json();
            else
                throw response;
        }).then(data => {
            this.addSeats(data);
        }).catch(response => {
            response.json().then(msg => {
                let text = '';
                if (!(typeof msg.Message === 'undefined')) {
                    text = " | " + msg.Message;
                }
                let error = { message: response.status + " " + response.statusText + text }
                console.log(error.message);
                this.props.actions.res_err(error.message);
            });
        })
    }

    addSeats(data) {
        this.state.seats = data;
        this.forceUpdate(); 
    }


    render() {
        let infoWin = this.props.state.reserve.info != '';
        let errWin = this.props.state.reserve.error != '';
        let errTxt = '';
        if (!errWin) {
            errTxt = this.props.state.reserve.info;
        }
        else {
            errTxt = this.props.state.reserve.error;
        }
        let listItems = this.listItems(this.state.seats);
        let isAuth = this.props.state.auth.Token != '';
        return (
            <div>
                <h2>Резервирование</h2>
                <div class="container">
                    <Info open={errWin || infoWin} isError={errWin} text={errTxt} onClose={this.alertClosed} />
                    <form onSubmit={this.handleSubmit} className="needs-validation" id="2">
                        <div class="row justify-content-end">
                            <div class="col-md-4 mr-auto">
                                <div class="container" hidden={isAuth}>
                                    <div class="border shadow-lg p-3 mb-5 bg-light rounded">
                                        <h2> Данные покупателя </h2>
                                        <div class="form-group">
                                            <label for="InputFirstName">Имя</label>
                                            <input type="text" class="form-control" id="InputFirstName"
                                                name="FirstName" placeholder="Введите имя"
                                                value={this.state.resData.FirstName} onChange={this.handleChange} required={!isAuth}/>
                                        </div>
                                        <div class="form-group">
                                            <label for="InputSecondName">Фамилия</label>
                                            <input type="text" class="form-control" id="InputSecondName"
                                                name="SecondName" placeholder="Введите фамилию"
                                                value={this.state.resData.SecondName} onChange={this.handleChange} required={!isAuth}/>
                                        </div>
                                        <div class="form-group">
                                            <label for="InputPassport">Паспорт</label>
                                            <input type="text" class="form-control" id="InputPassport"
                                                name="Passport" placeholder="Введите номер паспорта"
                                                value={this.state.resData.Passport} onChange={this.handleChange} required={!isAuth}/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 ml-auto">
                                <h2> Выбор места </h2>
                                <div class="container">
                                    <div class="list-group">{listItems}</div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <Auth />
                                <div class="container">
                                    <div class="border shadow-lg p-3 mb-5 bg-light rounded">
                                        <h2> Карта для оплаты </h2>
                                        <div class="form-group">
                                            <label for="InputCardNum">Номер карты</label>
                                            <input type="cardNum" class="form-control" id="InputCardNum"
                                                name="Card" placeholder="Введите номер карты" pattern="^[1-9][0-9]*$"
                                                value={this.state.resData.Card} onChange={this.handleChange} required />
                                        </div>
                                        <div class="form-group">
                                            <label for="InputSumm">Сумма</label>
                                            <input type="summ" class="form-control" id="InputSumm"
                                                name="Summ" placeholder="Введите пароль" pattern="^[1-9][0-9]*$"
                                                value={this.state.resData.Summ} onChange={this.handleChange} required />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-primary">
                                Купить
                            </button>
                        </div>
                    </form>
                </div>
            </div >
        );
    }
}

export default connect(
    (state) => {
        const { reserve, auth } = state;
        return {
            state: { reserve, auth }
        };
    },
    (dispatch) => {
        return {
            actions: bindActionCreators(Object.assign({}, actionCreators), dispatch)
        };
    }
)(Reserve);
