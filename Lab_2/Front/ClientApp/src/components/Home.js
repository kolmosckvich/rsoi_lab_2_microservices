﻿import React from 'react';
import { connect } from 'react-redux';

const Home = props => (
  <div>
      <h2>Категорическое приветствие</h2>

      <div class="text-center">Интерфейс максимально недружественный, так что маршрутизация проводится через адресную строку</div>
  </div>
);

export default connect()(Home);
