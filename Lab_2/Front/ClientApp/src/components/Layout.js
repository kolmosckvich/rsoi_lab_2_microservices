﻿import React from 'react';
import { Container } from 'reactstrap';
import './Layout.css'

export default props => (
    <div>
        <h1>МПС</h1>
        <Container>
            {props.children}
        </Container>
    </div>
);
