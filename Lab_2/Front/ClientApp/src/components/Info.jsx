﻿import React, { Component } from "react";
import Modal from 'react-responsive-modal';

class ModalWindow extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        if (this.props.isError)
            return (
                <Modal role="alert" classNames={{ modal: "w-100 alert alert-danger" }} open={this.props.open} onClose={this.props.onClose}>
                    <h2>Error!</h2>
                    <hr />
                    <span className="pl-2">{this.props.text}</span>
                </Modal>
            );
        else
            return (
                <Modal role="info" classNames={{ modal: "w-100 alert alert-light" }} open={this.props.open} onClose={this.props.onClose}>
                    <h2>Information</h2>
                    <hr />
                    <span className="pl-2">{this.props.text}</span>
                    <button type="button" className="btn btn-success float-right mt-3" onClick={this.props.onClose}>Ok!</button>
                </Modal>
            );
    }
}

export default ModalWindow;