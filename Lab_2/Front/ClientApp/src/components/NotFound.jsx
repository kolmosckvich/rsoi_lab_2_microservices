﻿import React, { Component } from "react";
import img_404 from "../img/404_7.jpg"

class NotFound extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div class="text-center">
                    <img src={img_404} class="image-center" alt="404..." />
                </div>
            </div>
        );
    }
}


export default NotFound;