﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/AuthReducer';
import Info from '../components/Info';

const AUTH_IP = 'http://127.0.0.1:5002/api/auth';
const md5 = require("blueimp-md5");

class Auth extends Component {

    constructor(props) {
        super(props);

        this.state = {
            error: '',
            authData: {
                Username: '',
                Password: '',
                Token:''
            }
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.alertClosed = this.alertClosed.bind(this);
        this.handleOAuth = this.handleOAuth.bind(this);
    }

    alertClosed(event) {
        this.props.actions.auth_conf();
    }

    handleOAuth(event) {
        window.location.href = 'https://localhost:5012/api/oauth?redirUrl=https://localhost:5016/oacallback';
    }


    handleChange(event) {
        const field = event.target.name;
        const authData = this.state.authData;
        authData[field] = event.target.value;
        return this.setState({ authData: authData });
    }

    handleSubmit(event) {
        event.preventDefault();
        let ad = {
            Username: this.state.authData.Username,
            Password: this.state.authData.Password,
        }

        fetch(`${AUTH_IP}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                Username: ad.Username,
                Password: md5(ad.Password)
            })
        }).then(response => {
            if (response.ok)
                return response.json();
            else
                throw response;
        }).then(data => {
            this.props.actions.auth_succ(data, ad.Username);
        }).catch(response => {
            response.json().then(msg => {
                let text = '';
                if (!(typeof msg.Message === 'undefined')) {
                    text = " | " + msg.Message;
                }
                let error = { message: response.status + " " + response.statusText + text}
                console.log(error.message);
                this.props.actions.auth_err(error.message);
            });
        })
    }

    render() {
        let infoWin = this.props.state.auth.info != '';
        let errWin = this.props.state.auth.error != '';
        let errTxt = '';
        if (!errWin) {
            errTxt = this.props.state.auth.info;
        }
        else {
            errTxt = this.props.state.auth.error;
        }
        if (this.props.state.auth.Token != '') {
            return (
                <div>
                    <div class="container">
                        <div class="border shadow-lg p-3 mb-5 bg-light rounded">
                            <h2>Успешный вход</h2>
                        </div>
                    </div>
                </div >
            );
        }
        else {
            return (
                <div>
                    <Info open={errWin || infoWin} isError={errWin} text={errTxt} onClose={this.alertClosed} />
                    <div class="container">
                        <div class="border shadow-lg p-3 mb-5 bg-light rounded">
                            <h2>Вход</h2>
                            <form onSubmit={this.handleSubmit} className="needs-validation" id="1">
                                <div class="form-group">
                                    <label for="InputUsername">Имя пользователя</label>
                                    <input type="text" class="form-control" id="InputUsername"
                                        name="Username" placeholder="Введите имя пользователя"
                                        value={this.state.authData.Username} onChange={this.handleChange} required
                                    />
                                </div>
                                <div class="form-group">
                                    <label for="InputPassword">Пароль</label>
                                    <input type="password" class="form-control" id="InputPassword"
                                        name="Password" placeholder="Введите пароль"
                                        value={this.state.authData.Password} onChange={this.handleChange} required
                                    />
                                </div>
                                <div class="row justify-content-between">
                                    <div class="col-4">
                                        <div class="text-center">
                                            <button type="submit" class="btn btn-primary">
                                                Вход
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="text-center">
                                            <button type="button" class="btn btn-primary" onClick={this.handleOAuth}>
                                                OAuth
                                            </button>
                                        </div>
                                    </div>
                                </div>
                               
                            </form>
                        </div>
                    </div>
                </div >
            );
        }
    }
}

export default connect(
    (state) => {
        const { auth } = state;
        return {
            state: { auth }
        };
    },
    (dispatch) => {
        return {
            actions: bindActionCreators(Object.assign({}, actionCreators), dispatch)
        };
    }
)(Auth);
