﻿import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { actionCreators } from '../store/AuthReducer';

const OAUTH_IP = 'http://127.0.0.1:5002/api/oauth';
const OAUTH_TOK_IP = 'http://127.0.0.1:5002/api/oauth/token';
const OAUTH_TOK_REF_IP = 'http://127.0.0.1:5002/api/oauth/refresh';

class OAuth extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let ad = this.props.location.search;
        let a = ad.indexOf('=');
        let tok = ad.slice(a + 1);
        let date =
        {
            Token: tok,
        }
        fetch(`${OAUTH_TOK_IP}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(date)
        }).then(response => {
            if (response.ok)
                return response.json();
            else
                throw response;
        }).then(data => {
            this.props.actions.oauth_req(data);
            let timerId = setTimeout(this.Refresh, data.Expires, data.RefreshToken, this.props.actions);
            this.props.history.push("/reserve");
        }).catch(response => {
            response.json().then(msg => {
                let text = '';
                if (!(typeof msg.Message === 'undefined')) {
                    text = " | " + msg.Message;
                }
                let error = { message: response.status + " " + response.statusText + text }
                console.log(error.message);
                this.props.actions.auth_err(error.message);
            });
        })
    }

    Refresh(refresh, actions) {
        let date =
        {
            RefreshToken: refresh,
        }
        fetch(`${OAUTH_TOK_REF_IP}`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(date)
        }).then(response => {
            if (response.ok)
                return response.json();
            else
                throw response;
        }).then(data => {
            actions.oauth_req(data);
            let timerId = setTimeout(this.Refresh, data.Expires, data.RefreshToken, actions);
        }).catch(response => {
            response.json().then(msg => {
                let text = '';
                if (!(typeof msg.Message === 'undefined')) {
                    text = " | " + msg.Message;
                }
                let error = { message: response.status + " " + response.statusText + text }
                console.log(error.message);
                actions.auth_err(error.message);
            });
        })
    }

    render() {
        return (
            <div>
                <div class="text-center">
                    <h3>Processing...</h3>
                </div>
            </div>
        );
    }
}

export default connect(
    (state) => {
        const { auth } = state;
        return {
            state: { auth }
        };
    },
    (dispatch) => {
        return {
            actions: bindActionCreators(Object.assign({}, actionCreators), dispatch)
        };
    }
)(OAuth);