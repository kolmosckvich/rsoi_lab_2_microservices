﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CentralServiceLib.Data
{
    [JsonObject]
    public class PrintData
    {
        [JsonRequired]
        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonRequired]
        [JsonProperty("SecondName")]
        public string SecondName { get; set; }

        [JsonRequired]
        [JsonProperty("Passport")]
        public string Passport { get; set; }

        [JsonRequired]
        [JsonProperty("SeatID")]
        public long SeatID { get; set; }

        [JsonRequired]
        [JsonProperty("SeatCoupe")]
        public int SeatCoupe { get; set; }

        [JsonRequired]
        [JsonProperty("SeatNum")]
        public int SeatNum { get; set; }
    }
}
