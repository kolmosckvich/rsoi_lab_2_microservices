﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSLib.Data
{
    [JsonObject]
    public class RegistrData
    {
        [JsonRequired]
        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonRequired]
        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonRequired]
        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonRequired]
        [JsonProperty("SecondName")]
        public string SecondName { get; set; }

        [JsonRequired]
        [JsonProperty("Passport")]
        public string Passport { get; set; }

        public RegistrData(string username, string password, string firstname, string secondname, string passport)
        {
            Username = username;
            Password = password;
            FirstName = firstname;
            SecondName = secondname;
            Passport = passport;
        }
    }
}
