﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CentralServiceLib.Data
{
    [JsonObject]
    public class BillData
    {
        [JsonRequired]
        [JsonProperty("Passport")]
        public string Passport { get; set; }

        [JsonRequired]
        [JsonProperty("Summ")]
        public int Summ { get; set; }

        [JsonProperty("DateTime")]
        public string DateTime { get; set; }

        [JsonRequired]
        [JsonProperty("Card")]
        public string Card { get; set; }
    }
}
