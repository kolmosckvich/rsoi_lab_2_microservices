﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSLib.Data
{
    [JsonObject]
    public class TokenData
    {
        [JsonRequired]
        [JsonProperty("Token")]
        public string Token { get; set; }

        public TokenData(string token)
        {
            Token = token;
        }
    }
}
