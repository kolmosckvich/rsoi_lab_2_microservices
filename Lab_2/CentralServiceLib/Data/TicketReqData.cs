﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CentralServiceLib.Data
{
    [JsonObject]
    public class TicketReqData
    {
        [JsonRequired]
        [JsonProperty("Passport")]
        public string Passport { get; set; }

        [JsonProperty("TicketID")]
        public long TicketID { get; set; }
    }
}
