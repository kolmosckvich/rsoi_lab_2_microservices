﻿using CSLib.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CentralServiceLib.Data
{
    [JsonObject]
    public class TokenCombReserveData
    {
        [JsonRequired]
        [JsonProperty("Token")]
        public string Token { get; set; }

        [JsonRequired]
        [JsonProperty("SeatID")]
        public long SeatID { get; set; }

        [JsonRequired]
        [JsonProperty("Summ")]
        public int Summ { get; set; }

        [JsonRequired]
        [JsonProperty("Card")]
        public string Card { get; set; }
    }
}
