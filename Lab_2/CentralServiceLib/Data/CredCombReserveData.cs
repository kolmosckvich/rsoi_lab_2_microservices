﻿using CSLib.Data;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CentralServiceLib.Data
{
    [JsonObject]
    public class CredCombReserveData
    {
        [JsonRequired]
        [JsonProperty("FirstName")]
        public string FirstName { get; set; }

        [JsonRequired]
        [JsonProperty("SecondName")]
        public string SecondName { get; set; }

        [JsonRequired]
        [JsonProperty("Passport")]
        public string Passport { get; set; }

        [JsonRequired]
        [JsonProperty("SeatID")]
        public long SeatID { get; set; }

        [JsonRequired]
        [JsonProperty("Summ")]
        public int Summ { get; set; }

        [JsonRequired]
        [JsonProperty("Card")]
        public string Card { get; set; }
    }
}
