﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CentralServiceLib.Data
{
    [JsonObject]
    public class TicketData
    {
        [JsonRequired]
        [JsonProperty("DateTime")]
        public string DateTime { get; set; }

        [JsonProperty("TicketID")]
        public long TicketID { get; set; }

        [JsonProperty("PrintData")]
        public PrintData PrintData { get; set; }
    }
}
