﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CentralServiceLib.Data
{
    [JsonObject]
    public class ReserveData
    {
        [JsonRequired]
        [JsonProperty("Passport")]
        public string Passport { get; set; }

        [JsonProperty("Reserved")]
        public bool Reserved { get; set; }

        [JsonProperty("Billed")]
        public bool Billed { get; set; }

        [JsonRequired]
        [JsonProperty("SeatID")]
        public long SeatID { get; set; }

        [JsonProperty("SeatCoupe")]
        public int SeatCoupe { get; set; }

        [JsonProperty("SeatNum")]
        public int SeatNum { get; set; }

        public ReserveData(string passport, long seatID, bool reserved = false, bool billed = false, 
                           int seatCoupe = 0, int seatNum = 0)
        {
            Passport = passport;
            Reserved = reserved;
            Billed = billed;
            SeatID = seatID;
            SeatCoupe = seatCoupe;
            SeatNum = seatNum;
        }
    }
}
