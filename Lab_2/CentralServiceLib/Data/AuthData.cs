﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CSLib.Data
{
    [JsonObject]
    public class AuthData
    {
        [JsonRequired]
        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonRequired]
        [JsonProperty("Password")]
        public string Password { get; set; }
    }
}
