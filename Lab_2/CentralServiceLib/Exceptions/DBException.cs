﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CentralServiceLib.Exceptions
{
    public class DBException : Exception
    {
        public DBException() : base()
        {

        }

        public DBException(string message):base(message)
        {

        }
    }
}
