﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CentralServiceLib.Exceptions
{
    public class IncorrectDataException : Exception
    {
        public IncorrectDataException() : base()
        {

        }

        public IncorrectDataException(string message) : base(message)
        {

        }
    }
}
