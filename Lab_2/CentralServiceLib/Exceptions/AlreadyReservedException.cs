﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CentralServiceLib.Exceptions
{
    public class AlreadyReservedException : Exception
    {
        public AlreadyReservedException() : base()
        {

        }

        public AlreadyReservedException(string message) : base(message)
        {

        }
    }
}
