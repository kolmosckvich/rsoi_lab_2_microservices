﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace CentralServiceLib
{
    [JsonObject]
    public class Error
    {
        [JsonProperty("Message")]
        public string Message { get; set; }

        public Error(string msg)
        {
            Message = msg;
        }
    }
}
