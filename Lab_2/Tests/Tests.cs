using AuthService.Controllers;
using AuthService.Model.Managers;
using AuthService.Model.Services;
using CSLib.Data;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Moq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using ReserveService.Model.Services;
using CentralServiceLib.Data;
using System.Collections.Generic;
using ReserveService.Controllers;
using BillService.Model.Services;
using BillService.Controllers;
using PrintService.Model.Services;
using PrintService.Controllers;
using System.Text;
using Gateway.Model.Services;
using Gateway.Controllers;
using System.IO;
using CentralServiceLib;

namespace Tests
{
    [TestClass]
    public class Tests
    {
        [TestMethod]
        public void TestHash()
        {
            string h1 = Hash.HashId("ss", DateTime.Today);
            string h2 = Hash.HashId("ss", DateTime.Today);
            string h3 = Hash.HashId("sa", DateTime.Today);

            Assert.AreEqual(h1, h2);
            Assert.AreNotEqual(h1, h3);
        }

        [TestMethod]
        public void TestTokenManager()
        {
            AuthData user_1 = new AuthData() { Username = "ss", Password = "123" };
            AuthData user_2 = new AuthData() { Username = "sa", Password = "321" };
            string t_1 = TokenManager.RegisterUserToken(user_1);
            string t_2 = TokenManager.RegisterUserToken(user_1);
            Assert.IsTrue(TokenManager.CheckUser(t_1));
            Assert.AreNotEqual(t_1, t_2);
            Assert.AreEqual(user_1.Username, TokenManager.GetAuthData(new TokenData(t_1)).Username);
        }

        [TestMethod]
        public void TestAuthApi()
        {
            var mservice = new Mock<IAuthService>();
            mservice.Setup(a => a.Authorize(It.IsAny<AuthData>()))
                    .Returns(Task.FromResult(new TokenData("0123")));

            mservice.Setup(a => a.GetRegData(It.IsAny<TokenData>()))
                    .Returns(Task.FromResult(new RegistrData("1", "2", "3", "4", "5")));

            AuthController contr = new AuthController(mservice.Object);
            var res = contr.Get("123").Result;

            var okResult = res as ObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult is OkObjectResult);
            Assert.IsInstanceOfType(okResult.Value, typeof(RegistrData));
            Assert.AreEqual((okResult.Value as RegistrData).Password, "2");
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);


            res = contr.Post(new AuthData() { Username = "1", Password = "2"}).Result;

            okResult = res as ObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult is OkObjectResult);
            Assert.IsInstanceOfType(okResult.Value, typeof(TokenData));
            Assert.AreEqual((okResult.Value as TokenData).Token, "0123");
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);
        }

        [TestMethod]
        public void TestRegApi()
        {
            var mservice = new Mock<IRegisterService>();
            mservice.Setup(a => a.Register(It.IsAny<RegistrData>()))
                    .Returns(Task.FromResult(new TokenData("0123")));

            RegistrationController contr = new RegistrationController(mservice.Object);
            var res = contr.Post(new RegistrData("1", "2", "3", "4", "5")).Result;

            var okResult = res as ObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult is OkObjectResult);
            Assert.IsInstanceOfType(okResult.Value, typeof(TokenData));
            Assert.AreEqual((okResult.Value as TokenData).Token, "0123");
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);


        }

        [TestMethod]
        public void TestReserveApi()
        {
            var mservice = new Mock<IReserveService>();
            var rd = new ReserveData("1", 1, true, true);
            List<ReserveData> ret = new List<ReserveData>();
            ret.Add(rd);

            mservice.Setup(a => a.GetAllSeats())
                    .Returns(ret);

            mservice.Setup(a => a.ClearReserve(It.IsAny<ReserveData>()));

            mservice.Setup(a => a.GetSeat(It.IsAny<long>()))
                    .Returns(rd);

            mservice.Setup(a => a.ReserveFinally(It.IsAny<ReserveData>()));

            ReserveController contr = new ReserveController(mservice.Object);
            var res = contr.Get().Result;

            var okResult = res as ObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult is OkObjectResult);
            Assert.IsInstanceOfType(okResult.Value, typeof(List<ReserveData>));
            Assert.AreEqual((okResult.Value as List<ReserveData>)[0].Reserved, true);
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);


            res = contr.Get(1).Result;

            okResult = res as ObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult is OkObjectResult);
            Assert.IsInstanceOfType(okResult.Value, typeof(ReserveData));
            Assert.AreEqual((okResult.Value as ReserveData).Reserved, true);
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);

            res = contr.Post(ret[0]).Result;

            var ok = res as OkResult;

            Assert.IsNotNull(ok);
            Assert.IsTrue(ok is OkResult);
            Assert.AreEqual(StatusCodes.Status200OK, ok.StatusCode);
        }

        [TestMethod]
        public void TestBillApi()
        {
            var mservice = new Mock<IBillService>();
            mservice.Setup(a => a.DoBillTransaction(It.IsAny<BillData>()));

            BillData bd = new BillData()
                { Card = "123", DateTime = "0:0", Summ = 123, Passport = "1" };
            BillController contr = new BillController(mservice.Object);
            var res = contr.Post(bd).Result;

            var ok = res as OkResult;

            Assert.IsNotNull(ok);
            Assert.IsTrue(ok is OkResult);
            Assert.AreEqual(StatusCodes.Status200OK, ok.StatusCode);
        }

        [TestMethod]
        public void TestPrintApi()
        {
            PrintData pd = new PrintData()
                {FirstName = "1", SecondName = "2", Passport = "123", SeatID = 1, SeatCoupe = 1, SeatNum = 1};
            PrintController contr = new PrintController(new PrintingService());
            var res = contr.Print(pd).Result;

            var result = res as FileContentResult;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.ContentType, "application/pdf");
            Assert.IsNotNull(result.FileContents);
        }

        [TestMethod]
        public void TestGatewayApi()
        {
            var mservice = new Mock<IGatewayService>();

            var td = new TokenData("0123");
            var regd = new RegistrData("1", "2", "3", "4", "5");
            var resd = new ReserveData("1", 1, true, true);
            var ret = new List<ReserveData>();
            ret.Add(resd);
            var pd = new PrintData()
                { FirstName = "1", SecondName = "2", Passport = "123", SeatID = 1, SeatCoupe = 1, SeatNum = 1 };
            var bd = new BillData()
                { Card = "123", DateTime = "0:0", Summ = 123, Passport = "1" };
            var ad = new AuthData() { Username = "1", Password = "2" };

            mservice.Setup(a => a.RegisterUser(It.IsAny<RegistrData>()))
                    .Returns(Task.FromResult(td));

            mservice.Setup(a => a.AuthUser(It.IsAny<AuthData>()))
                    .Returns(Task.FromResult(td));

            mservice.Setup(a => a.GetRegData(It.IsAny<TokenData>()))
                    .Returns(Task.FromResult(regd));

            mservice.Setup(a => a.GetSeats())
                    .Returns(Task.FromResult(ret));

            
            PrintingService ser = new PrintingService();
            var doc = ser.CreateDocument(pd);
            using (MemoryStream stream = new MemoryStream())
            {
                doc.Save(stream, false);
                mservice.Setup(a => a.FullyReserveSeat(It.IsAny<TokenCombReserveData>()))
                    .Returns(Task.FromResult(stream.ToArray()));
            }

            using (MemoryStream stream = new MemoryStream())
            {
                doc.Save(stream, false);
                mservice.Setup(a => a.FullyReserveSeat(It.IsAny<CredCombReserveData>()))
                    .Returns(Task.FromResult(stream.ToArray()));
            }

            GatewayController contr = new GatewayController(mservice.Object);
            var res = contr.GetRegisterData("123").Result;

            var okResult = res as ObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult is OkObjectResult);
            Assert.IsInstanceOfType(okResult.Value, typeof(RegistrData));
            Assert.AreEqual((okResult.Value as RegistrData).Password, "2");
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);


            res = contr.Authorize(ad).Result;

            okResult = res as ObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult is OkObjectResult);
            Assert.IsInstanceOfType(okResult.Value, typeof(TokenData));
            Assert.AreEqual((okResult.Value as TokenData).Token, "0123");
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);

            res = contr.Register(regd).Result;

            okResult = res as ObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult is OkObjectResult);
            Assert.IsInstanceOfType(okResult.Value, typeof(TokenData));
            Assert.AreEqual((okResult.Value as TokenData).Token, "0123");
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);

            res = contr.GetSeats().Result;

            okResult = res as ObjectResult;

            Assert.IsNotNull(okResult);
            Assert.IsTrue(okResult is OkObjectResult);
            Assert.IsInstanceOfType(okResult.Value, typeof(List<ReserveData>));
            Assert.AreEqual((okResult.Value as List<ReserveData>)[0].Reserved, true);
            Assert.AreEqual(StatusCodes.Status200OK, okResult.StatusCode);

            var cdr = new CredCombReserveData()
            {
                FirstName = regd.FirstName,
                Card = bd.Card,
                Passport = regd.Passport,
                SecondName = regd.SecondName,
                SeatID = resd.SeatID,
                Summ = bd.Summ
            };
            res = contr.ReserveSeatByCreds(cdr).Result;

            var result = res as FileContentResult;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.ContentType, "application/pdf");
            Assert.IsNotNull(result.FileContents);

            var tdr = new TokenCombReserveData()
            {
                Token = td.Token,
                Card = bd.Card,
                SeatID = resd.SeatID,
                Summ = bd.Summ
            };
            res = contr.ReserveSeatByToken(tdr).Result;

            result = res as FileContentResult;

            Assert.IsNotNull(result);
            Assert.AreEqual(result.ContentType, "application/pdf");
            Assert.IsNotNull(result.FileContents);
        }
    }
}
