﻿using CentralServiceLib.Data;
using ReserveService.Model.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReserveService.Model
{
    public class ReserveTimeout
    {
        private ReserveData data;
        private ReservationService service;

        public ReserveTimeout(ReserveData _data, ReservationService _service)
        {
            data = _data;
            service = _service;
        }

        public void Timeout()
        {
            service.ClearReserve(data);
        }

    }
}
