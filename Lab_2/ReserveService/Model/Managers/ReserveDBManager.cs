﻿using CentralServiceLib;
using CentralServiceLib.Data;
using CentralServiceLib.Exceptions;
using ReserveService.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ReserveService.Model.Managers
{
    public class ReserveDBManager : DBManager
    {
        public ReserveDBManager(string _connectionString) : base(_connectionString)
        {

        }

        public void UpdateReserveData(ReserveData data)
        {
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string cmdstr = $"UPDATE Reservations SET " +
                                    $"Username = '{data.Passport}', " +
                                    $"Reserved = '{data.Reserved}', " +
                                    $"Billed = '{data.Billed}' " +
                                    $"WHERE SeatID = {data.SeatID}";
                    var cmd = new SqlCommand(cmdstr, connection);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw new DBException(e.Message);
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                throw new DBException(e.Message);
            }

        }

        public ReserveData GetReserveData(long seatID)
        {
            ReserveData rd;
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string cmdstr = $"SELECT * FROM Reservations WHERE SeatID = '{seatID}'";
                    var cmd = new SqlCommand(cmdstr, connection);

                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (!rdr.HasRows)
                    {
                        throw new ReserveDBException();
                    }
                    else if (rdr.RecordsAffected > 1)
                    {
                        throw new ReserveDBException();
                    }
                    rdr.Read();
                    rd = new ReserveData(rdr["Username"].ToString(),
                                         (Int64)rdr["SeatID"],
                                         (bool)rdr["Reserved"],
                                         (bool)rdr["Billed"],
                                         (Int32)rdr["SeatCoupe"],
                                         (Int32)rdr["SeatNum"]);

                    rd.Passport = "Reserved";
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                throw new DBException(e.Message);
            }
            return rd;
        }

        public List<ReserveData> GetSeats()
        {
            List<ReserveData> rdList = new List<ReserveData>();
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string cmdstr = $"SELECT * FROM Reservations";
                    var cmd = new SqlCommand(cmdstr, connection);

                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (!rdr.HasRows)
                    {
                        throw new ReserveDBException();
                    }
                    while(rdr.Read())
                    {
                        ReserveData rd = new ReserveData(rdr["Username"].ToString(),
                                         (Int64)rdr["SeatID"],
                                         (bool)rdr["Reserved"],
                                         (bool)rdr["Billed"],
                                         (Int32)rdr["SeatCoupe"],
                                         (Int32)rdr["SeatNum"]);
                        rd.Passport = "";
                        rdList.Add(rd);
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                throw new DBException(e.Message);
            }
            rdList.RemoveRange(10, 26);
            return rdList;
        }
    }
}
