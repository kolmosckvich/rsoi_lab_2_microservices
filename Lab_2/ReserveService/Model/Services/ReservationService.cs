﻿using CentralServiceLib.Data;
using CentralServiceLib.Exceptions;
using ReserveService.Exceptions;
using ReserveService.Model.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace ReserveService.Model.Services
{
    public class ReservationService : IReserveService
    {
        private ReserveDBManager dbManager;

        private Dictionary<string, Timer> Timers = new Dictionary<string, Timer>();
        private TimeSpan timeout = TimeSpan.FromMinutes(20);

        public ReservationService()
        {
            string connectionString = AppConfiguration.ConnectionString;
            dbManager = new ReserveDBManager(connectionString);
        }

        public List<ReserveData> GetAllSeats()
        {
            return dbManager.GetSeats();
        }

        public ReserveData GetSeat(long seatID)
        {
            return dbManager.GetReserveData(seatID);
        }

        public void ReserveFinally(ReserveData data)
        {
            if(GetSeat(data.SeatID).Billed)
            {
                throw new AlreadyReservedException("Данное место уже зарезервировано");
            }
            data.Billed = true;
            data.Reserved = true;
            UpdateReserve(data);
        }

        public void ClearReserve(ReserveData data)
        {
            data.Passport = "";
            data.Billed = false;
            data.Reserved = false;
            UpdateReserve(data);
        }

        /*
        public void ReserveTime(ReserveData data)
        {
            data.Reserved = true;
            UpdateReserve(data);
        }
        */

        private void UpdateReserve(ReserveData data)
        {
            dbManager.UpdateReserveData(data);
        }
    }
}
