﻿using CentralServiceLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReserveService.Model.Services
{
    public interface IReserveService
    {
        List<ReserveData> GetAllSeats();

        ReserveData GetSeat(long seatID);

        void ReserveFinally(ReserveData data);

        void ClearReserve(ReserveData data);
    }
}
