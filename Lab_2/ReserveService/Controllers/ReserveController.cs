﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CentralServiceLib;
using CentralServiceLib.Data;
using CentralServiceLib.Exceptions;
using CSLib.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using ReserveService.Exceptions;
using ReserveService.Model.Managers;
using ReserveService.Model.Services;

namespace ReserveService.Controllers
{
    [Route("api/reserve")]
    [ApiController]
    public class ReserveController : Controller
    {
        private IReserveService reserveService;

        public ReserveController(IReserveService service)
        {
            if (service == null)
            {
                throw new ArgumentNullException();
            }
            reserveService = service;

        }

        [NonAction]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.RouteData.Values["action"].ToString() == "Auth")
                return;
            var headerValue = Request.Headers["MyAuth"];
            if (headerValue.Any() == false) context.Result = Unauthorized();
            var person = headerValue.ToString();
            if (!TokenManager.CheckUser(person)) context.Result = Unauthorized();
            return;
        }

        [HttpPost("auth")]
        public async Task<IActionResult> Auth([FromBody] AuthData data)
        {
            Console.WriteLine("Received data in auth post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            TokenData td = new TokenData("");
            try
            {
                if (data.Username == AppConfiguration.GatewayAppID && data.Password == AppConfiguration.GatewayAppSecret)
                {
                    string token = TokenManager.RegisterUserToken(data);
                    td = new TokenData(token);
                }
                else return Unauthorized();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");
            }

            Console.WriteLine("Sended data in auth post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(td));

            return Ok(td);
        }

        // GET: api/reserve
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            Console.WriteLine("Started reserve get method: ");

            try
            {
                List<ReserveData> seats = new List<ReserveData>();
                seats = reserveService.GetAllSeats();

                Console.WriteLine("Sended data in reserve get method: ");
                Console.WriteLine(JsonConvert.SerializeObject(seats));

                return Ok(seats);
            }
            catch(Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return StatusCode(500);
            }
        }

        // GET: api/reserve/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(long id)
        {
            Console.WriteLine($"Started reserve get method for id {id}: ");

            try
            {
                ReserveData seat = reserveService.GetSeat(id);

                Console.WriteLine($"Sended data in reserve get method for id {id}: ");
                Console.WriteLine(JsonConvert.SerializeObject(seat));

                return Ok(seat);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return StatusCode(500);
            }
        }

        // POST: api/reserve
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ReserveData data)
        {
            Console.WriteLine("Received data in reserve post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            try
            {
                reserveService.ReserveFinally(data);

                Console.WriteLine("Finished response in reserve post method with 200");

                return Ok();
            }
            catch (AlreadyReservedException e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return StatusCode(500, new Error(e.Message));
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return StatusCode(500);
            }
        }

        // Patch: api/reserve
        [HttpPatch]
        public async Task<IActionResult> Patch([FromBody] ReserveData data)
        {
            Console.WriteLine("Received data in reserve patch method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            try
            {
                reserveService.ClearReserve(data);

                Console.WriteLine("Finished response in reserve patch method with 200");

                return Ok();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return StatusCode(500);
            }
        }

        /*
        // PUT: api/Reserve/5
        [HttpPut]
        public async Task<IActionResult> Put([FromBody] ReserveData data)
        {
            try
            {
                reserveService.ReserveTime(data);
                return Ok();
            }
            catch (Exception e)
            {
                return StatusCode(500);
            }
        }
        */
    }
}
