﻿using CentralServiceLib.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ReserveService.Exceptions
{
    public class ReserveDBException : DBException
    {
        public ReserveDBException() : base()
        {

        }

        public ReserveDBException(string message) : base(message)
        {

        }
    }
}
