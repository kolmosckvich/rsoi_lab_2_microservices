﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Gateway.Model.Interfaces;
using Gateway.Model.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Http;
using Polly;
using Polly.Extensions.Http;

namespace Gateway
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            AppConfiguration.AuthPath = Configuration["AuthServicePath"];
            AppConfiguration.RegistrPath = Configuration["RegistrServicePath"];
            AppConfiguration.ReservePath = Configuration["ReserveServicePath"];
            AppConfiguration.BillPath = Configuration["BillServicePath"];
            AppConfiguration.PrintPath = Configuration["PrintServicePath"];
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddHttpClient<IAuthService, AuthService>(client =>
            {
                client.BaseAddress = new Uri(AppConfiguration.AuthPath);
            }).AddPolicyHandler(GetRetryPolicy())
              .AddPolicyHandler(GetCircuitBreakerPolicy());

            services.AddHttpClient<IRegisterService, RegisterService>(client =>
            {
                client.BaseAddress = new Uri(AppConfiguration.RegistrPath);
            }).AddPolicyHandler(GetRetryPolicy())
              .AddPolicyHandler(GetCircuitBreakerPolicy());

            services.AddHttpClient<IBillService, BillService>(client =>
            {
                client.BaseAddress = new Uri(AppConfiguration.BillPath);
            })/*.AddPolicyHandler(GetRetryPolicy())
              .AddPolicyHandler(GetCircuitBreakerPolicy())*/;

            services.AddHttpClient<IReserveService, ReserveService>(client =>
            {
                client.BaseAddress = new Uri(AppConfiguration.ReservePath);
            }).AddPolicyHandler(GetRetryPolicy())
              .AddPolicyHandler(GetCircuitBreakerPolicy());

            services.AddHttpClient<IPrintService, PrintService>(client =>
            {
                client.BaseAddress = new Uri(AppConfiguration.PrintPath);
            })/*.AddPolicyHandler(GetRetryPolicy())
              .AddPolicyHandler(GetCircuitBreakerPolicy())*/;


            services.AddTransient<IGatewayService, GatewayService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(builder => builder
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            //app.UseHttpsRedirection();
            app.UseMvc();
        }

        private static IAsyncPolicy<HttpResponseMessage> GetCircuitBreakerPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .CircuitBreakerAsync(3, TimeSpan.FromSeconds(10), 
                                    (ex, dur) => { Console.WriteLine("Circuit breaker opened"); }, 
                                    () => { Console.WriteLine("Circuit breaker reseted"); });
        }

        private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
        {
            return HttpPolicyExtensions
                .HandleTransientHttpError()
                .WaitAndRetryAsync(3, retryAttempt => TimeSpan.FromSeconds(Math.Pow(3, retryAttempt)), 
                                  (exception, timeSpan, retryCount, context) =>
                {
                    Console.WriteLine("Retry break through");
                });
        }
    }
}
