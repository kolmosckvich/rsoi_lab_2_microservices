﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway
{
    public static class AppConfiguration
    {
        public static string AuthPath { get; set; }
        public static string RegistrPath { get; set; }
        public static string PrintPath { get; set; }
        public static string ReservePath { get; set; }
        public static string BillPath { get; set; }

        public static string GatewayAppID = "123";
        public static string GatewayAppSecret = "777";

        public static string ReserveServiceToken = "";
        public static string BillServiceToken = "";
        public static string PrintServiceToken = "";

    }
}
