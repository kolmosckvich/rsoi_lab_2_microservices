﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Exceptions
{
    public class QueuedException : ServiceException
    {
        public QueuedException() : base()
        {

        }

        public QueuedException(string msg) : base(msg)
        {

        }
    }
}
