﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Exceptions
{
    public class ReserveException : ServiceException
    {
        public ReserveException() : base()
        {

        }

        public ReserveException(string msg) : base(msg)
        {

        }
    }
}
