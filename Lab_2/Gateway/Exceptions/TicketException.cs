﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Exceptions
{
    public class TicketException : ServiceException
    {
        public TicketException() : base()
        {

        }

        public TicketException(string msg) : base(msg)
        {

        }
    }
}
