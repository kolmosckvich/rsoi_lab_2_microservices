﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Exceptions
{
    public class BillException : ServiceException
    {
        public BillException() : base()
        {

        }

        public BillException(string msg) : base(msg)
        {

        }
    }
}
