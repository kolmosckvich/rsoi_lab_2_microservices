﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Exceptions
{
    public class ServiceException : Exception
    {
        public ServiceException():base()
        {

        }

        public ServiceException(string msg):base(msg)
        {

        }
    }
}
