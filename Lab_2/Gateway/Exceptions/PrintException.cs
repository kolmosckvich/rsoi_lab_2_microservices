﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Exceptions
{
    public class PrintException : ServiceException
    {
        public PrintException() : base()
        {

        }

        public PrintException(string msg) : base(msg)
        {

        }
    }
}
