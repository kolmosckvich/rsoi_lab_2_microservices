﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Exceptions
{
    public class RegistrationException : ServiceException
    {
        public RegistrationException() : base()
        {

        }

        public RegistrationException(string msg) : base(msg)
        {

        }
    }
}
