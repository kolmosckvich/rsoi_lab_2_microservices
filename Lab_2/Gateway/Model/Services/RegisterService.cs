﻿using CSLib.Data;
using Gateway.Exceptions;
using Gateway.Model.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Gateway.Model.Services
{
    public class RegisterService : IRegisterService
    {
        private readonly HttpClient client;

        public RegisterService(HttpClient _client)
        {
            client = _client;
        }

        public async Task<TokenData> RegisterUser(RegistrData data)
        {
            string content = JsonConvert.SerializeObject(data);
            StringContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
            var resp = await client.PostAsync("", httpContent);
            if (resp.IsSuccessStatusCode)
            {
                string ans = await resp.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<TokenData>(ans);
            }
            else
            {
                throw new RegistrationException();
            }
        }
    }
}
