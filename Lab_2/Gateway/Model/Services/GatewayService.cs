﻿using CentralServiceLib.Data;
using CentralServiceLib.Exceptions;
using CSLib.Data;
using Gateway.Exceptions;
using Gateway.Model.Interfaces;
using Gateway.Model.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Model.Services
{
    public class GatewayService : IGatewayService
    {
        private IAuthService authService;
        private IRegisterService regService;
        private IBillService billService;
        private IPrintService printService;
        private IReserveService reserveService;

        private QueueManager queue;

        public GatewayService(IAuthService auth, IRegisterService reg, IBillService bill, IReserveService res,  IPrintService print)
        {
            authService = auth;
            regService = reg;
            billService = bill;
            printService = print;
            reserveService = res;
            queue = new QueueManager(res, bill);
        }

        public async Task<TokenData> RegisterUser(RegistrData data)
        {
            TokenData td = await regService.RegisterUser(data);
            return td;
        }

        public async Task<TokenData> AuthUser(AuthData data)
        {
            TokenData td = await authService.AuthUser(data);
            return td;
        }

        public async Task<RegistrData> GetRegData(TokenData data)
        {
            RegistrData rd = await authService.GetRegData(data);
            return rd;
        }

        public async Task<List<ReserveData>> GetSeats()
        {
            List<ReserveData> seats = await reserveService.GetSeats();
            return seats;
        }

        public async Task<byte[]> FullyReserveSeat(TokenCombReserveData data)
        {
            checkData(data);
            RegistrData regData;
            TokenData td = new TokenData(data.Token);
            try
            {
                regData = await authService.GetRegData(td);
            }
            catch(RegistrationException e)
            {
                throw e;
            }

            ReserveData rdata = new ReserveData(regData.Passport, data.SeatID);

            BillData bdata = new BillData() { Passport = regData.Passport, Card = data.Card, Summ = data.Summ };

            if (!await reserveService.CheckSeat(data.SeatID))
            {
                throw new ReserveException("Seat already reserved");
            }

            try
            {
                await reserveService.ReserveSeat(rdata);
            }
            catch (AlreadyReservedException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                queue.AddReserveRequest(rdata, bdata);
                throw new QueuedException("Часть сервисов в данный момент недоступны. " +
                                          "Ваш запрос был поставлен в очередь на выполнение. " +
                                          "В случае удовлетворения запроса, Вы сможете распечатать ваш билет в любой из касс МПС. " +
                                          "Приносим извинения за причинённые неудобства");
            }
            
            ReserveData seatInfo = await reserveService.GetSeat(rdata.SeatID);

            PrintData pd = new PrintData()
            {
                FirstName = regData.FirstName,
                Passport = regData.Passport,
                SecondName = regData.SecondName,
                SeatCoupe = seatInfo.SeatCoupe,
                SeatID = rdata.SeatID,
                SeatNum = seatInfo.SeatNum
            };

            await printService.SaveTicket(pd);

            try
            {
                await billService.Bill(bdata);
            }
            catch (Exception e)
            {
                queue.AddBillRequest(bdata);
                throw new QueuedException("Часть сервисов в данный момент недоступны. " +
                                          "Ваш запрос был поставлен в очередь на выполнение. " +
                                          "Вы сможете распечатать ваш билет в любой из касс МПС. " +
                                          "Приносим извинения за причинённые неудобства");
            }

            byte[] ticket = await printService.PrintTicket(pd);

            return ticket;
        }
            

        public async Task<byte[]> FullyReserveSeat(CredCombReserveData data)
        {
            checkData(data);
            ReserveData rdata = new ReserveData(data.Passport, data.SeatID);
            if (!await reserveService.CheckSeat(data.SeatID))
            {
                throw new ReserveException("Seat already reserved");
            }

            BillData bdata = new BillData() { Passport = data.Passport, Card = data.Card, Summ = data.Summ };

            await reserveService.ReserveSeat(rdata);
            ReserveData seatInfo = await reserveService.GetSeat(rdata.SeatID);

            PrintData pd = new PrintData()
            {
                FirstName = data.FirstName,
                Passport = data.Passport,
                SecondName = data.SecondName,
                SeatCoupe = seatInfo.SeatCoupe,
                SeatID = data.SeatID,
                SeatNum = seatInfo.SeatNum
            };

            try
            {
                await printService.SaveTicket(pd);
            }
            catch(Exception e)
            {
                await reserveService.ClearReserveSeat(rdata);
                throw e;
            }

            try
            {
                await billService.Bill(bdata);
            }
            catch(Exception e)
            {
                await printService.DeleteTicket(pd);
                await reserveService.ClearReserveSeat(rdata);
                throw e;
            }

            byte[] ticket = await printService.PrintTicket(pd);

            return ticket;
        }

        public async Task<byte[]> PrintExistTicket(TicketReqData data)
        {
            byte[] ticket = await printService.PrintExistTicket(data);
            return ticket;
        }

        private void checkData(TokenCombReserveData data)
        {
            string excMsg = "";
            if (data.Summ < 0)
            {
                excMsg += "Summ must be higher than zero\n";
            }
            if (data.SeatID < 0)
            {
                excMsg += "SeatID must be higher than zero\n";
            }
            if (data.Card == "")
            {
                excMsg += "Card must be have at least 1 character length!\n";
            }
            if (excMsg != "")
                throw new IncorrectDataException(excMsg);
        }

        private void checkData(CredCombReserveData data)
        {
            string excMsg = "";
            if (data.Summ < 0)
            {
                excMsg += "Summ must be higher than zero\n";
            }
            if (data.SeatID < 0)
            {
                excMsg += "SeatID must be higher than zero\n";
            }
            if (data.Card == "")
            {
                excMsg += "Card must be have at least 1 character length!\n";
            }
            if (data.FirstName == "")
            {
                excMsg += "FirstName must be have at least 1 character length!\n";
            }
            if (data.SecondName == "")
            {
                excMsg += "SecondName must be have at least 1 character length!\n";
            }
            if (data.Passport == "")
            {
                excMsg += "Passport must be have at least 1 character length!\n";
            }
            if (excMsg != "")
                throw new IncorrectDataException(excMsg);
        }

    }
}
