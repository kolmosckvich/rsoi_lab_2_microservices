﻿using CentralServiceLib.Data;
using CSLib.Data;
using Gateway.Exceptions;
using Gateway.Model.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Gateway.Model.Services
{
    public class BillService : IBillService
    {
        private readonly HttpClient client;

        public BillService(HttpClient _client)
        {
            client = _client;
        }

        public async Task Auth()
        {
            try
            {
                string content = JsonConvert.SerializeObject(
                                                                new AuthData()
                                                                {
                                                                    Username = AppConfiguration.GatewayAppID,
                                                                    Password = AppConfiguration.GatewayAppSecret
                                                                }
                                                            );
                StringContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                var resp = await client.PostAsync(client.BaseAddress + "/auth", httpContent);
                if (resp.IsSuccessStatusCode)
                {
                    string ans = await resp.Content.ReadAsStringAsync();
                    TokenData td = JsonConvert.DeserializeObject<TokenData>(ans);
                    AppConfiguration.BillServiceToken = td.Token;
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Add("MyAuth", AppConfiguration.BillServiceToken);
                }
                else
                {
                    throw new BillException("Сервис оплаты недоступен");
                }
            }
            catch (Exception e)
            {
                throw new BillException("Сервис оплаты недоступен");
            }
        }

        public async Task<bool> Bill(BillData data)
        {
            try
            {
                string content = JsonConvert.SerializeObject(data);
                StringContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                var resp = await client.PostAsync("", httpContent);
                if (resp.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (resp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await Auth();
                    return await Bill(data);
                }
                else
                {
                    throw new BillException("Сервис оплаты недоступен");
                }
            }
            catch(Exception e)
            {
                throw new BillException("Сервис оплаты недоступен");
            }
        }
    }
}
