﻿using CentralServiceLib.Data;
using CSLib.Data;
using Gateway.Exceptions;
using Gateway.Model.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Gateway.Model.Services
{
    public class PrintService : IPrintService
    {
        private readonly HttpClient client;

        public PrintService(HttpClient _client)
        {
            client = _client;
        }

        public async Task Auth()
        {
            try
            {
                string content = JsonConvert.SerializeObject(
                                                                new AuthData()
                                                                {
                                                                    Username = AppConfiguration.GatewayAppID,
                                                                    Password = AppConfiguration.GatewayAppSecret
                                                                }
                                                            );
                StringContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                var resp = await client.PostAsync(client.BaseAddress + "/auth", httpContent);
                if (resp.IsSuccessStatusCode)
                {
                    string ans = await resp.Content.ReadAsStringAsync();
                    TokenData td = JsonConvert.DeserializeObject<TokenData>(ans);
                    AppConfiguration.PrintServiceToken = td.Token;
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Add("MyAuth", AppConfiguration.PrintServiceToken);
                }
                else
                {
                    throw new TicketException("Сервис печати билета недоступен");
                }
            }
            catch (Exception e)
            {
                throw new TicketException("Сервис печати билета недоступен");
            }
        }

        public async Task<byte[]> PrintTicket(PrintData data)
        {
            try
            {
                string content = JsonConvert.SerializeObject(data);
                StringContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                var resp = await client.PostAsync(client.BaseAddress + "/print", httpContent);
                if (resp.IsSuccessStatusCode)
                {
                    byte[] res = await resp.Content.ReadAsByteArrayAsync();
                    return res;
                }
                else if (resp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await Auth();
                    return await PrintTicket(data);
                }
                else
                {
                    throw new PrintException("Сервис печати билета недоступен");
                }
            }
            catch(Exception e)
            {
                throw new PrintException("Сервис печати билета недоступен");
            }
        }

        public async Task<bool> SaveTicket(PrintData data)
        {
            try
            {
                string content = JsonConvert.SerializeObject(data);
                StringContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                var resp = await client.PostAsync(client.BaseAddress + "/ticket", httpContent);
                if (resp.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (resp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await Auth();
                    return await SaveTicket(data);
                }
                else
                {
                    throw new TicketException("Сервис печати билета недоступен");
                }
            }
            catch(Exception e)
            {
                throw new TicketException("Сервис печати билета недоступен");
            }
            
        }

        public async Task<bool> DeleteTicket(PrintData data)
        {
            try
            {
                string content = JsonConvert.SerializeObject(data);
                StringContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                var resp = await client.PatchAsync(client.BaseAddress + "/ticket", httpContent);
                if (resp.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (resp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await Auth();
                    return await DeleteTicket(data);
                }
                else
                {
                    throw new TicketException("Сервис печати билета недоступен");
                }
            }
            catch(Exception e)
            {
                throw new TicketException("Сервис печати билета недоступен");
            }
        }

        public async Task<byte[]> PrintExistTicket(TicketReqData data)
        {
            try
            {
                string content = JsonConvert.SerializeObject(data);
                StringContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");

                UriBuilder ub = new UriBuilder(client.BaseAddress + "/ticket");
                ub.Query = $"TicketID={data.TicketID}&Passport={data.Passport}";
                Uri addr = ub.Uri;
                string uri = addr.ToString();

                var resp = await client.GetAsync(uri);
                if (resp.IsSuccessStatusCode)
                {
                    byte[] res = await resp.Content.ReadAsByteArrayAsync();
                    return res;
                }
                else if (resp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await Auth();
                    return await PrintExistTicket(data);
                }
                else
                {
                    throw new TicketException("Сервис печати билета недоступен");
                }
            }
            catch (Exception e)
            {
                throw new TicketException("Сервис печати билета недоступен");
            }
        }
    }
}
