﻿using CentralServiceLib.Data;
using CSLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Model.Services
{
    public interface IGatewayService
    {
        Task<TokenData> RegisterUser(RegistrData data);

        Task<TokenData> AuthUser(AuthData data);

        Task<RegistrData> GetRegData(TokenData data);

        Task<List<ReserveData>> GetSeats();

        Task<byte[]> FullyReserveSeat(TokenCombReserveData data);

        Task<byte[]> FullyReserveSeat(CredCombReserveData data);

        Task<byte[]> PrintExistTicket(TicketReqData data);
    }
}
