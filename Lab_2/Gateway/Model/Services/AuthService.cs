﻿using CSLib.Data;
using Gateway.Exceptions;
using Gateway.Model.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Gateway.Model.Services
{
    public class AuthService : IAuthService
    {
        private readonly HttpClient client;

        public AuthService(HttpClient _client)
        {
            client = _client;
        }

        public async Task<TokenData> AuthUser(AuthData data)
        {
            try
            {
                string content = JsonConvert.SerializeObject(data);
                StringContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                var resp = await client.PostAsync("", httpContent);
                if (resp.IsSuccessStatusCode)
                {
                    string ans = await resp.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<TokenData>(ans);
                }
                else
                {
                    throw new RegistrationException();
                }
            }
            catch(Exception e)
            {
                throw new RegistrationException();
            }
        }

        public async Task<RegistrData> GetRegData(TokenData data)
        {
            try
            {
                UriBuilder ub = new UriBuilder(client.BaseAddress);
                ub.Query = $"token={data.Token}";
                Uri addr = ub.Uri;
                string uri = addr.ToString();

                var resp = await client.GetAsync(uri);
                if (resp.IsSuccessStatusCode)
                {
                    string ans = await resp.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<RegistrData>(ans);
                }
                else
                {
                    throw new RegistrationException();
                }
            }
            catch(Exception e)
            {
                throw new RegistrationException();
            }
            
        }

    }
}
