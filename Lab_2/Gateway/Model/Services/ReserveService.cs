﻿using CentralServiceLib;
using CentralServiceLib.Data;
using CentralServiceLib.Exceptions;
using CSLib.Data;
using Gateway.Exceptions;
using Gateway.Model.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Gateway.Model.Services
{
    public class ReserveService : IReserveService
    {
        private readonly HttpClient client;

        public ReserveService(HttpClient _client)
        {
            client = _client;
        }

        public async Task Auth()
        {
            try
            {
                string content = JsonConvert.SerializeObject(
                                                                new AuthData() 
                                                                {
                                                                    Username = AppConfiguration.GatewayAppID, 
                                                                    Password = AppConfiguration.GatewayAppSecret 
                                                                }
                                                            );
                StringContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                var resp = await client.PostAsync(client.BaseAddress + "/auth", httpContent);
                if (resp.IsSuccessStatusCode)
                {
                    string ans = await resp.Content.ReadAsStringAsync();
                    TokenData td = JsonConvert.DeserializeObject<TokenData>(ans);
                    AppConfiguration.ReserveServiceToken = td.Token;
                    client.DefaultRequestHeaders.Clear();
                    client.DefaultRequestHeaders.Add("MyAuth", AppConfiguration.ReserveServiceToken);
                }
                else
                {
                    throw new ReserveException("Сервис резервирования недоступен");
                }
            }
            catch (Exception e)
            {
                throw new ReserveException("Сервис резервирования недоступен");
            }
        }

        public async Task<List<ReserveData>> GetSeats()
        {
            try
            {
                var resp = await client.GetAsync("");
                if (resp.IsSuccessStatusCode)
                {
                    string ans = await resp.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<List<ReserveData>>(ans);
                }
                else if(resp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await Auth();
                    return await GetSeats();
                }
                else
                {
                    throw new ReserveException("Сервис резервирования недоступен");
                }
            }
            catch(Exception e)
            {
                throw new ReserveException("Сервис резервирования недоступен");
            }
        }

        public async Task<ReserveData> GetSeat(long SeatID)
        {
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", AppConfiguration.ReserveServiceToken);
                var resp = await client.GetAsync(client.BaseAddress + $"/{SeatID}");
                if (resp.IsSuccessStatusCode)
                {
                    string ans = await resp.Content.ReadAsStringAsync();
                    return JsonConvert.DeserializeObject<ReserveData>(ans);
                }
                else if (resp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await Auth();
                    return await GetSeat(SeatID);
                }
                else
                {
                    throw new ReserveException("Сервис резервирования недоступен");
                }
            }
            catch (Exception e)
            {
                throw new ReserveException("Сервис резервирования недоступен");
            }
        }

        public async Task<bool> ReserveSeat(ReserveData data)
        {
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", AppConfiguration.ReserveServiceToken);
                string content = JsonConvert.SerializeObject(data);
                StringContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                var resp = await client.PostAsync("", httpContent);
                if (resp.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (resp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await Auth();
                    return await ReserveSeat(data);
                }
                else
                {
                    throw new ReserveException("Сервис резервирования недоступен");
                }
            }
            catch (Exception e)
            {
                throw new ReserveException("Сервис резервирования недоступен");
            }
        }

        public async Task<bool> ClearReserveSeat(ReserveData data)
        {
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", AppConfiguration.ReserveServiceToken);
                string content = JsonConvert.SerializeObject(data);
                StringContent httpContent = new StringContent(content, Encoding.UTF8, "application/json");
                var resp = await client.PatchAsync("", httpContent);
                if (resp.IsSuccessStatusCode)
                {
                    return true;
                }
                else if (resp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await Auth();
                    return await ClearReserveSeat(data);
                }
                else
                {
                    if(resp.Content != null)
                    {
                        string ans = await resp.Content.ReadAsStringAsync();
                        string errMsg = JsonConvert.DeserializeObject<Error>(ans).Message;
                        if (errMsg != "")
                        {
                            throw new AlreadyReservedException(errMsg);
                        }
                    }
                    throw new ReserveException("Сервис резервирования недоступен");
                }
            }
            catch (AlreadyReservedException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw new ReserveException("Сервис резервирования недоступен");
            }
        }

        public async Task<bool> CheckSeat(long SeatID)
        {
            try
            {
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", AppConfiguration.ReserveServiceToken);
                var resp = await client.GetAsync(client.BaseAddress + $"/{SeatID}");
                if (resp.IsSuccessStatusCode)
                {
                    string ans = await resp.Content.ReadAsStringAsync();
                    ReserveData rd = JsonConvert.DeserializeObject<ReserveData>(ans);
                    if (rd.Reserved || rd.Billed)
                    {
                        return false;
                    }
                    return true;
                }
                else if (resp.StatusCode == HttpStatusCode.Unauthorized)
                {
                    await Auth();
                    return await CheckSeat(SeatID);
                }
                else
                {
                    throw new ReserveException("Сервис резервирования недоступен");
                }
            }
            catch (Exception e)
            {
                throw new ReserveException("Сервис резервирования недоступен");
            }
        }
    }
}
