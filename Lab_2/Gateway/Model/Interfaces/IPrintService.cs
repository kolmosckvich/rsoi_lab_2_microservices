﻿using CentralServiceLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Model.Interfaces
{
    public interface IPrintService
    {
        Task<byte[]> PrintTicket(PrintData data);

        Task<bool> SaveTicket(PrintData data);

        Task<bool> DeleteTicket(PrintData data);

        Task<byte[]> PrintExistTicket(TicketReqData data);
    }
}
