﻿using CentralServiceLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Model.Interfaces
{
    public interface IBillService
    {
        Task<bool> Bill(BillData data);
    }
}
