﻿using CentralServiceLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Model.Interfaces
{
    public interface IReserveService
    {
        Task<List<ReserveData>> GetSeats();

        Task<ReserveData> GetSeat(long SeatID);

        Task<bool> ReserveSeat(ReserveData data);

        Task<bool> ClearReserveSeat(ReserveData data);

        Task<bool> CheckSeat(long SeatID);
    }
}
