﻿using CSLib.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Gateway.Model.Interfaces
{
    public interface IAuthService
    {
        Task<TokenData> AuthUser(AuthData data);

        Task<RegistrData> GetRegData(TokenData data);
    }
}
