﻿using CentralServiceLib.Data;
using CentralServiceLib.Exceptions;
using Gateway.Model.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Gateway.Model.Managers
{
    class ReserveAndBillData
    {
        public ReserveData rd { get; }
        public BillData bd { get; }

        public ReserveAndBillData(ReserveData res, BillData bill)
        {
            bd = bill;
            rd = res;
        }
    }

    public class QueueManager
    {
        private ConcurrentQueue<ReserveAndBillData> reserveQueue = new ConcurrentQueue<ReserveAndBillData>();
        private ConcurrentQueue<BillData> billQueue = new ConcurrentQueue<BillData>();

        private const int pendDelayMs = 3000;

        private IReserveService reserveService;
        private IBillService billService;

        public QueueManager(IReserveService res, IBillService bill)
        {
            reserveService = res;
            billService = bill;
        }

        public void AddReserveRequest(ReserveData rd, BillData bd)
        {
            reserveQueue.Enqueue(new ReserveAndBillData(rd, bd));
            if(reserveQueue.Count == 1)
            {
                Task.Run(startReservePend);
            }
    }

        public void AddBillRequest(BillData data)
        {
            billQueue.Enqueue(data);
            if (billQueue.Count == 1)
            {
                Task.Run(startBillPend);
            }
        }

        private async Task startReservePend()
        {
            ReserveAndBillData data;
            while(reserveQueue.TryPeek(out data))
            {
                try
                {
                    await reserveService.ReserveSeat(data.rd);
                    reserveQueue.TryDequeue(out data);
                    AddBillRequest(data.bd);
                }
                catch(AlreadyReservedException e)
                {
                    reserveQueue.TryDequeue(out data);
                }
                catch(Exception e)
                {
                    Thread.Sleep(pendDelayMs);
                }
            }
        }

        private async Task startBillPend()
        {
            BillData data;
            while (billQueue.TryPeek(out data))
            {
                try
                {
                    await billService.Bill(data);
                    billQueue.TryDequeue(out data);
                }
                catch (Exception e)
                {
                    Thread.Sleep(pendDelayMs);
                }
            }
        }
    }
}
