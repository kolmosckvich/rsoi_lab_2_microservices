﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CentralServiceLib;
using CentralServiceLib.Data;
using CentralServiceLib.Exceptions;
using CSLib.Data;
using Gateway.Exceptions;
using Gateway.Model.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Gateway.Controllers
{
    [Route("gateway")]
    [ApiController]
    public class GatewayController : ControllerBase
    {
        private IGatewayService gatewayService;

        public GatewayController(IGatewayService service)
        {
            if (service == null)
            {
                throw new ArgumentNullException();
            }
            gatewayService = service;
        }

        // POST: api/Gateway/reg
        [HttpPost("reg")]
        public async Task<IActionResult> Register([FromBody] RegistrData data)
        {
            Console.WriteLine("Received data in gateway reg post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            try
            {
                TokenData td = await gatewayService.RegisterUser(data);

                Console.WriteLine("Sended data in gateway reg post method: ");
                Console.WriteLine(JsonConvert.SerializeObject(td));

                return Ok(td);
            }
            catch(Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return processException(e);
            }
        }

        // GET: api/Gateway/auth
        [HttpGet("auth")]
        public async Task<IActionResult> GetRegisterData([FromQuery] string token)
        {
            Console.WriteLine("Received data in gateway auth get method: ");
            Console.WriteLine(token);

            try
            {
                RegistrData rd = await gatewayService.GetRegData(new TokenData(token));

                Console.WriteLine("Sended data in gateway auth get method: ");
                Console.WriteLine(JsonConvert.SerializeObject(rd));

                return Ok(rd);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return processException(e);
            }
        }

        // POST: api/Gateway/auth
        [HttpPost("auth")]
        public async Task<IActionResult> Authorize([FromBody] AuthData data)
        {
            Console.WriteLine("Received data in gateway auth post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            try
            {
                TokenData td = await gatewayService.AuthUser(data);

                Console.WriteLine("Sended data in gateway auth post method: ");
                Console.WriteLine(JsonConvert.SerializeObject(td));

                return Ok(td);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return processException(e);
            }
        }

        // POST: api/Gateway/reserve
        [HttpGet("reserve")]
        public async Task<IActionResult> GetSeats()
        {
            Console.WriteLine("Started gateway reserve get method: ");
            try
            {
                List<ReserveData> rd = await gatewayService.GetSeats();

                Console.WriteLine("Sended data in gateway reserve post method: ");
                Console.WriteLine(JsonConvert.SerializeObject(rd));

                //return BadRequest("фу!");
                return Ok(rd);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return processException(e);
            }
        }

        // POST: api/Gateway/ticket/token
        [HttpPost("ticket/token")]
        public async Task<IActionResult> ReserveSeatByToken([FromBody] TokenCombReserveData data)
        {
            Console.WriteLine("Started gateway ticket post method: ");

            try
            {
                byte[] ticket = await gatewayService.FullyReserveSeat(data);

                Console.WriteLine("Sended file in gateway ticket post method: ");

                return File(ticket, "application/pdf");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return processException(e);
            }
        }

        // POST: api/Gateway/ticket/cred
        [HttpPost("ticket/cred")]
        public async Task<IActionResult> ReserveSeatByCreds([FromBody] CredCombReserveData data)
        {
            Console.WriteLine("Started gateway ticket post method: ");

            try
            {
                byte[] ticket = await gatewayService.FullyReserveSeat(data);

                Console.WriteLine("Sended file in gateway ticket post method: ");

                return File(ticket, "application/pdf");
            }
            catch(PrintException e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}\nCan't print ticket");
                return processException(e);
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return processException(e);
            }
        }

        // GET: api/Gateway/ticket/print
        [HttpGet("ticket/print")]
        public async Task<IActionResult> PrintExistTicket([FromQuery] long ticketID, [FromQuery] string passport)
        {

            TicketReqData data = new TicketReqData() { Passport = passport, TicketID = ticketID };
            Console.WriteLine("Started gateway ticket post method: ");

            try
            {
                byte[] ticket = await gatewayService.PrintExistTicket(data);

                Console.WriteLine("Sended file in gateway ticket post method: ");

                return File(ticket, "application/pdf");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");
                return processException(e);
                
            }
        }

        private IActionResult processException(Exception exc)
        {
            try
            {
                throw exc;
            }
            catch(QueuedException e)
            {
                return StatusCode(500, new Error(e.Message));
            }
            catch (PrintException e)
            {
                return StatusCode(500, new Error("Место было зарезервировано, но сервис печати недоступен в данный момент. "+
                                                 "Вы можете распечатать ваш билет в любой из касс МПС. "+
                                                 "Приносим извинения за причинённые неудобства"));
            }
            catch (IncorrectDataException e)
            {
                return BadRequest(new Error(e.Message));
            }
            catch (RegistrationException e)
            {
                return Forbid();
            }
            catch (Exception e)
            {
                return StatusCode(500, new Error(e.Message));
            }
        }

    }
}
