﻿using CentralServiceLib.Data;
using PdfSharpCore.Drawing;
using PdfSharpCore.Drawing.Layout;
using PdfSharpCore.Pdf;
using PrintService.Exceptions;
using PrintService.Model.Managers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintService.Model.Services
{
    public class PrintingService : IPrintService
    {
        private TicketsDBManager dbManager;

        public PrintingService()
        {
            string connectionString = AppConfiguration.ConnectionString;
            dbManager = new TicketsDBManager(connectionString);
        }

        public void SaveTicket(PrintData data)
        {
            dbManager.AddTicketData(data);
        }

        public void DeleteTicket(PrintData data)
        {
            dbManager.RemoveTicketData(data);
        }

        public List<TicketData> GetTickets(TicketReqData data)
        {
            return dbManager.GetTickets(data);
        }

        public PdfDocument PrintExistTicket(TicketReqData data)
        {
            TicketData td = dbManager.GetTicket(data);
            return CreateDocument(td.PrintData);
        }

        public PdfDocument CreateDocument(PrintData data)
        {
            string firstName = data.FirstName;
            string secondName = data.SecondName;
            string passport = data.Passport;
            long seatId = data.SeatID;
            int seatCoupe = data.SeatCoupe;
            int seatNum = data.SeatNum;

            using (MemoryStream stream = new MemoryStream())
            {
                String toPrint = $"First Name: {firstName}\n" +
                                 $"Second Name: {secondName}\n" +
                                 $"Passport: {passport}\n" +
                                 $"Seat ID: {seatId}\n" +
                                 $"Coupe: {seatCoupe}\n" +
                                 $"Seat Num: {seatNum}";
                PdfDocument document = new PdfDocument();
                PdfPage page = document.AddPage();
                XGraphics gfx = XGraphics.FromPdfPage(page);
                XTextFormatter tf = new XTextFormatter(gfx);
                Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
                XFont font = new XFont("Arial", 20, XFontStyle.Bold);
                XRect rect = new XRect(0, 0, page.Width, page.Height);
                tf.DrawString(toPrint, font, XBrushes.Black, rect, XStringFormats.TopLeft);
                
                return document;
            }
        }
    }
}
