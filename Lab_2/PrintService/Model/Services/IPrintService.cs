﻿using CentralServiceLib.Data;
using PdfSharpCore.Pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintService.Model.Services
{
    public interface IPrintService
    {
        PdfDocument CreateDocument(PrintData data);

        void SaveTicket(PrintData data);

        void DeleteTicket(PrintData data);

        List<TicketData> GetTickets(TicketReqData data);

        PdfDocument PrintExistTicket(TicketReqData data);
    }
}
