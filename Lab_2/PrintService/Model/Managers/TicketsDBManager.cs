﻿using CentralServiceLib;
using CentralServiceLib.Data;
using CentralServiceLib.Exceptions;
using PrintService.Exceptions;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PrintService.Model.Managers
{
    public class TicketsDBManager : DBManager
    {
        public TicketsDBManager(string _connectionString) : base(_connectionString)
        {

        }

        public void AddTicketData(PrintData data)
        {
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string dt = DateTime.Now.ToString("g");
                    string cmdstr = $"INSERT INTO Tickets (FirstName, SecondName, Passport, " +
                                    $"SeatID, SeatCoupe, SeatNum, DateTime) " +
                                    $"VALUES ('{data.FirstName}', '{data.SecondName}', '{data.Passport}'," +
                                    $" '{data.SeatID}', '{data.SeatCoupe}', '{data.SeatNum}', '{dt}')";
                    var cmd = new SqlCommand(cmdstr, connection);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw new DBException(e.Message);
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                throw new DBException(e.Message);
            }

        }

        public void RemoveTicketData(PrintData data)
        {
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string cmdstr = $"DELETE FROM Tickets WHERE FirstName = '{data.FirstName}' AND " +
                                    $"SecondName = '{data.SecondName}' AND Passport = '{data.Passport}' AND " +
                                    $"SeatID = '{data.SeatID}' ";
                    var cmd = new SqlCommand(cmdstr, connection);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception e)
                    {
                        throw new DBException(e.Message);
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                throw new DBException(e.Message);
            }

        }

        public TicketData GetTicket(TicketReqData data)
        {
            TicketData td;
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string cmdstr = $"SELECT * FROM Tickets WHERE Passport = '{data.Passport}' AND TicketID = '{data.TicketID}'";
                    var cmd = new SqlCommand(cmdstr, connection);

                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (!rdr.HasRows)
                    {
                        throw new TicketReqException("Can't find ticket with those passport and ticket id");
                    }
                    rdr.Read();
                    td = new TicketData()
                    {
                        TicketID = (Int64)(rdr["TicketID"]),
                        DateTime = rdr["DateTime"].ToString(),
                        PrintData = new PrintData()
                        {
                            FirstName = rdr["FirstName"].ToString(),
                            SecondName = rdr["SecondName"].ToString(),
                            Passport = rdr["Passport"].ToString(),
                            SeatID = (Int64)(rdr["SeatID"]),
                            SeatCoupe = (Int32)(rdr["SeatCoupe"]),
                            SeatNum = (Int32)(rdr["SeatNum"])
                        }
                    };
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                throw new DBException(e.Message);
            }
            return td;
        }

        public List<TicketData> GetTickets(TicketReqData data)
        {
            List<TicketData> tdList = new List<TicketData>();
            try
            {
                using (connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    string cmdstr = $"SELECT * FROM Tickets";
                    var cmd = new SqlCommand(cmdstr, connection);

                    SqlDataReader rdr = cmd.ExecuteReader();
                    if (!rdr.HasRows)
                    {
                        throw new TicketReqException("Can't find tickets with this passport data");
                    }
                    while (rdr.Read())
                    {
                        TicketData td = new TicketData()
                        {
                            TicketID = (Int64)(rdr["TicketID"]),
                            DateTime = rdr["DateTime"].ToString(),
                            PrintData = new PrintData()
                            {
                                FirstName = rdr["FirstName"].ToString(),
                                SecondName = rdr["SecondName"].ToString(),
                                Passport = rdr["Passport"].ToString(),
                                SeatID = (Int64)(rdr["SeatID"]),
                                SeatCoupe = (Int32)(rdr["SeatCoupe"]),
                                SeatNum = (Int32)(rdr["SeatNum"])
                            }
                        };
                        tdList.Add(td);
                    }
                    connection.Close();
                }
            }
            catch (Exception e)
            {
                throw new DBException(e.Message);
            }
            return tdList;
        }

    }
}
