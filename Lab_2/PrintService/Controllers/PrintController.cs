﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CentralServiceLib.Data;
using CSLib.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using PdfSharpCore.Pdf;
using PrintService.Model.Managers;
using PrintService.Model.Services;

namespace PrintService.Controllers
{
    [Route("api")]
    [ApiController]
    public class PrintController : Controller
    {
        private IPrintService printService;

        public PrintController(IPrintService service)
        {
            if (service == null)
            {
                throw new ArgumentNullException();
            }
            printService = service;
        }

        [NonAction]
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.RouteData.Values["action"].ToString() == "Auth")
                return;
            var headerValue = Request.Headers["MyAuth"];
            if (headerValue.Any() == false) context.Result = Unauthorized();
            var person = headerValue.ToString();
            if (!TokenManager.CheckUser(person)) context.Result = Unauthorized();
            return;
        }

        [HttpPost("auth")]
        public async Task<IActionResult> Auth([FromBody] AuthData data)
        {
            Console.WriteLine("Received data in auth post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            TokenData td = new TokenData("");
            try
            {
                if (data.Username == AppConfiguration.GatewayAppID && data.Password == AppConfiguration.GatewayAppSecret)
                {
                    string token = TokenManager.RegisterUserToken(data);
                    td = new TokenData(token);
                }
                else return Unauthorized();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");
            }

            Console.WriteLine("Sended data in auth post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(td));

            return Ok(td);
        }

        // Post: api/ticket
        [HttpPost("ticket")]
        public async Task<IActionResult> SaveTicket([FromBody] PrintData data)
        {
            Console.WriteLine("Received data in ticket post method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            try
            {
                printService.SaveTicket(data);

                Console.WriteLine("Ended ticket post with 200");

                return Ok();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return StatusCode(500);
            }
        }

        // Patch: api/ticket
        [HttpPatch("ticket")]
        public async Task<IActionResult> DeleteTicket([FromBody] PrintData data)
        {
            Console.WriteLine("Received data in ticket patch method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            try
            {
                printService.DeleteTicket(data);

                Console.WriteLine("Ended ticket patch with 200");

                return Ok();
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return StatusCode(500);
            }
        }

        // Post: api/ticket
        [HttpGet("ticket")]
        public async Task<IActionResult> PrintExistTicket([FromQuery] long ticketID, [FromQuery] string passport)
        {
            TicketReqData data = new TicketReqData() { Passport = passport, TicketID = ticketID };
            Console.WriteLine("Received data in ticket print method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            try
            {
                PdfDocument doc = printService.PrintExistTicket(data);
                using (MemoryStream stream = new MemoryStream())
                {
                    doc.Save(stream, false);

                    Console.WriteLine("Sended file in ticket print method");

                    return File(stream.ToArray(), "application/pdf");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return StatusCode(500);
            }
        }

        // Get: api/Print
        [HttpPost("print")]
        public async Task<IActionResult> Print([FromBody] PrintData data)
        {
            Console.WriteLine("Received data in print get method: ");
            Console.WriteLine(JsonConvert.SerializeObject(data));

            try
            {
                PdfDocument doc = printService.CreateDocument(data);
                using (MemoryStream stream = new MemoryStream())
                {
                    doc.Save(stream, false);

                    Console.WriteLine("Sended file in print get method");

                    return File(stream.ToArray(), "application/pdf");
                }
            }
            catch(Exception e)
            {
                Console.WriteLine($"Finished response with error: {e.Message}");

                return StatusCode(500);
            }
            
        }
    }
}
