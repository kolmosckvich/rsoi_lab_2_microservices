﻿using CentralServiceLib.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PrintService.Exceptions
{
    public class TicketReqException : DBException
    {
        public TicketReqException() : base()
        {

        }

        public TicketReqException(string message) : base(message)
        {

        }
    }
}
